package database;

import database.helper.configFactory.ConfigHelper;
import io.ebean.EbeanServer;
import io.ebean.EbeanServerFactory;
import io.ebean.config.ServerConfig;
import io.ebean.config.dbplatform.mysql.MySqlPlatform;
import io.ebean.datasource.DataSourceConfig;
import io.vertx.core.json.JsonObject;

public enum SqlConfigFactory {

    MASTER;

    private EbeanServer masterServer;

    public EbeanServer getServer() {
        switch (this) {
            default:
            case MASTER:
                return getMasterServer();
        }
    }

    public void generateMasterServer() {
        try {
            JsonObject jsonObject = ConfigHelper.INSTANCE.getMysqlConfig().getJsonObject("master");
            System.out.println(jsonObject.toString());
            DataSourceConfig dataSourceConfig = new DataSourceConfig();
            dataSourceConfig.setDriver(jsonObject.getString("driver"));
            dataSourceConfig.setUrl(jsonObject.getString("url"));
            dataSourceConfig.setUsername(jsonObject.getString("username"));
            dataSourceConfig.setPassword(jsonObject.getString("password"));
            //dataSourceConfig.setMaxConnections(MAX_CONNECTIONS);
            ServerConfig serverConfig = new ServerConfig();
            serverConfig.setName("master");
            serverConfig.setRegister(true);
            serverConfig.setDefaultServer(true);
            serverConfig.setDdlGenerate(false);
            serverConfig.setDdlRun(false);
            serverConfig.addPackage("models");
            serverConfig.setDataSourceConfig(dataSourceConfig);
            serverConfig.setDatabasePlatform(new MySqlPlatform());
            masterServer = EbeanServerFactory.create(serverConfig);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private EbeanServer getMasterServer() {
        if (masterServer == null) {
            generateMasterServer();
        }
        return masterServer;
    }
}

