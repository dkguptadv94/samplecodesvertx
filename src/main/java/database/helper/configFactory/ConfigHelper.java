package database.helper.configFactory;

import io.vertx.core.json.JsonObject;

public enum ConfigHelper {

    INSTANCE;

    public JsonObject getMysqlConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("sql");
    }
/*
    public JsonObject getMainConfig(){
        return ConfigManager.INSTANCE.getMainConfig();
    }

    public JsonObject getApplicationConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("config");
    }

    public JsonObject getEkaConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("apis").getJsonObject("EKA");
    }

    public JsonObject getMailConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("apis").getJsonObject("mail");
    }

    public JsonObject getFileUploadConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("apis").getJsonObject("fileuplaod").getJsonObject("blackhole");
    }

    public JsonObject getFeedServerConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("apis").getJsonObject("feedserver");
    }
*/
}

