package cityMallTest.csvCreater;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class CsvFileCreater {
    public static void main(String[] args) {
        try {
            PrintWriter pw=new PrintWriter(new File("/Volumes/WorkSpaceDice//book.csv"));
            StringBuilder sb=new StringBuilder();

            sb.append("SrNo");
            sb.append(",");
            sb.append("BookName");
            sb.append(",");
            sb.append("Category");
            sb.append("\r\n");

            sb.append("1");
            sb.append(",");
            sb.append("BookName 1");
            sb.append(",");
            sb.append("Category 1");
            sb.append("\r\n");

            sb.append("2");
            sb.append(",");
            sb.append("BookName 2");
            sb.append(",");
            sb.append("Category 2");
            sb.append("\r\n");

            sb.append("3");
            sb.append(",");
            sb.append("BookName 3");
            sb.append(",");
            sb.append("Category 3");
            sb.append("\r\n");

            pw.write(sb.toString());
            pw.close();
            System.out.println("Finished");

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
