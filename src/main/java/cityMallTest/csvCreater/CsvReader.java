package cityMallTest.csvCreater;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CsvReader {
    public static void main(String[] args) {
        BufferedReader br=null;
        String line=" ";
        String file="/Volumes/WorkSpaceDice//book.csv";
        try {
            br = new BufferedReader(new FileReader(file));
            while((line= br.readLine())!=null){
                String[] split = line.split(",");
                for (String index:split){
                    System.out.println(index);
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
