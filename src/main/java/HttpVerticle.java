import controller.HotelsRouter;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.core.http.HttpMethod;
import io.vertx.rxjava.core.http.HttpServer;
import io.vertx.rxjava.ext.web.Router;
import io.vertx.rxjava.ext.web.handler.TimeoutHandler;
import io.vertx.rxjava.ext.web.handler.BodyHandler;

public class HttpVerticle extends AbstractVerticle {
    @Override
    public void start(){
        HttpServer httpServer = vertx.createHttpServer();
        Router router = Router.router(vertx);
        router.route().handler(io.vertx.rxjava.ext.web.handler.CorsHandler.create("*")
                .allowedMethod(io.vertx.core.http.HttpMethod.GET)
                .allowedMethod(io.vertx.core.http.HttpMethod.POST)
                .allowedMethod(io.vertx.core.http.HttpMethod.PATCH)
                .allowedMethod(io.vertx.core.http.HttpMethod.PUT)
                .allowedMethod(io.vertx.core.http.HttpMethod.DELETE)
                .allowedMethod(io.vertx.core.http.HttpMethod.OPTIONS)
                .allowedHeader("Access-Control-Request-Method")
                .allowedHeader("Access-Control-Allow-Credentials")
                .allowedHeader("Access-Control-Allow-Origin")
                .allowedHeader("Access-Control-Allow-Headers")
                .allowedHeader("Authorization")
                .allowedHeader("Content-Type"));

        router.route("/*").handler(TimeoutHandler.create(75000, 503)).handler(event -> {
            if (event.request().method().equals(HttpMethod.GET) || event.request().method().equals(HttpMethod.DELETE)) {
                event.next();
            } else if (event.request().getHeader("content-type") != null && event.request().getHeader("content-type").toLowerCase().contains("application/json")) {
                io.vertx.rxjava.ext.web.handler.BodyHandler.create().handle(event);
            } else if (event.request().getHeader("Content-Type") != null && event.request().getHeader("Content-Type").toLowerCase().contains("application/json")) {
                BodyHandler.create().handle(event);
            } else {
                event.next();
            }
        });
/**
 * SearchCity Details API calling router.....
 */
        router.mountSubRouter("/cities", HotelsRouter.INSTANCE.router(vertx));


        httpServer.requestHandler(router::accept).listen(8181);
    }
}
