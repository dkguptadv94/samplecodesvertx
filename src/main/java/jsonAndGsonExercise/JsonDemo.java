package jsonAndGsonExercise;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonDemo {
    public static void main(String[] args) {
        JsonDemo jsonDemo=new JsonDemo();
        jsonDemo.jsonStuffs();
    }

    private void jsonStuffs() {

        Map<String,String> map=new HashMap<>();
        map.put("stuId","123");
        map.put("stuName","test");
        map.put("stuAge","23");
        System.out.println("From Map : "+map);

        JSONObject jsonObject=new JSONObject(map);
//        jsonObject.put("stuId","123");
//        jsonObject.put("stuName","test");
//        jsonObject.put("stuAge","23");
        System.out.println("From Json : "+jsonObject);


        List<String>list=new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        System.out.println("from ListArray : "+list);

        JSONArray jsnArray=new JSONArray(list);
//        jsnArray.put("a");
//        jsnArray.put("b");
//        jsnArray.put("c");
//        jsnArray.put("d");
        System.out.println("from JsonArray :"+jsnArray);
        jsonObject.put("alphabet",jsnArray);
        System.out.println("Entire alphabet : "+jsonObject);
    }
}
