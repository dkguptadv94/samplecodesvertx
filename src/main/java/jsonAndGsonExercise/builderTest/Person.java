package jsonAndGsonExercise.builderTest;

import lombok.Builder;
import lombok.Getter;

@Getter
public class Person {
    private String name;
    private int age;
    private boolean isActive=true;
    private String role="Admin";
    @Builder
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
