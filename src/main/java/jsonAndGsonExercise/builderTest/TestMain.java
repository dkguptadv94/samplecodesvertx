package jsonAndGsonExercise.builderTest;

public class TestMain {
    public static void main(String[] args) {
        /**
         * Using here User
         * Class level Builder
         */
        User user1 = User.builder().name("Ganesh").age("27").build();
        User user2 = User.builder().name("Sourabh").build();
        User user3 = User.builder().build();

        System.out.println(user1.getName()+"*****"+user1.getAge());
        System.out.println(user2.getName()+"*****"+user2.getAge());
        System.out.println(user3.getName()+"*****"+user3.getAge());

        /**
         * Play with Person
         * Constructor level Builder
         */
        Person pitter = Person.builder().name("Pitter").age(20).build();
        System.out.println(pitter.getName()+" "+pitter.getAge()+" "+pitter.getRole()+" "+pitter.isActive());


        /**
         * play with Customer
         * Method level Builder
         */

        Customer ben = Customer.builder().name("Ben").age(33).build();
        System.out.println(ben.getName()+" "+ben.getAge()+" "+ben.isActive()+" "+ben.getCity());
    }
}
