package jsonAndGsonExercise;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonExam01 {
    public static void main(String[] args) {
        JsonExam01 jsonExam01=new JsonExam01();
        jsonExam01.generateJsonData();
        jsonExam01.readFromJsonData();
    }

    private JSONObject generateJsonData() {
        JSONObject jsonInner=new JSONObject();
        JSONObject jsonObject=new JSONObject();
        JSONArray jsonArray=new JSONArray();

        jsonInner.put("hotelId","424023");
        jsonInner.put("destinationId","1506246");
        jsonInner.put("pointOfSaleId","HCOM_US");
        jsonInner.put("currencyCode","USD");
        jsonInner.put("occupancyKey","A2");

        jsonObject.put("pdpHeader",jsonInner);
        jsonObject.put("clientToken","Es626BNAVcX_QZkxp8P5_j3LzpE");

        jsonArray.put("latitude");
        jsonArray.put("longitude");
        jsonObject.put("coordinates",jsonArray);
        System.out.println("JsonResponse : "+jsonObject);
        return jsonObject;
    }
    public void readFromJsonData(){
        JSONObject jsonObjectForRead = generateJsonData();
        JSONObject innerJson1 = jsonObjectForRead.getJSONObject("pdpHeader");
        System.out.println("form outer JsonResponse : \n"+jsonObjectForRead.getJSONObject("pdpHeader"));
        System.out.println("form inner JsonResponse : \n"+innerJson1.getString("hotelId"));
        System.out.println("form outer JsonResponse : \n"+jsonObjectForRead.getString("clientToken"));

    }
}
