package jsonAndGsonExercise.deSerialization;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

public class DemoJson {
    public static void main(String[] args) {
        Student student1=new Student("DK","DICE001",1212121212,new Address("Street-1","Madhuban Calony","Pune","112233"));
        Student student2=new Student("Sourabh","DICE002",1232323232,new Address("Street-1","Madhuban Calony","Pune","112233"));

        List<Student> students = Arrays.asList(student1, student2);

        Gson gson=new Gson();
        String json01 = gson.toJson(student1);
        System.out.println(json01);

        System.out.println("List of object for Json response ");
        String listOfObj = gson.toJson(students);
        System.out.println(listOfObj);
    }
}
