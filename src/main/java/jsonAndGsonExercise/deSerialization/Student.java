package jsonAndGsonExercise.deSerialization;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private String name;
    private String stuId;
    private long phone;
    private Address address;
}
