package jsonAndGsonExercise.deSerialization;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JacksonDemo {
    public static void main(String[] args) {
        getJson(new Student("DK","DICE003",333333,new Address("Street-04","Madhuban Calony","Pune","404441")));
        getJson(new Student("Ganesh","DICE001",111111,new Address("Madhuban Colony Street no-05","Pune","Mumbai","233305")));
    }

    /**
     * Json conversion with help of Jackson-API
     * @param student
     */
    private static void getJson(Student student) {
        ObjectMapper mapper = new ObjectMapper();
        String jacksonRes = null;
        try {
                    jacksonRes=mapper.writeValueAsString(student);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Response with help of Gson : " + jacksonRes);
        getObj(jacksonRes);
    }

    /**
     * De-Serialization
     * @param str
     */
    private static void getObj(String str) {
        ObjectMapper objectMapper=new ObjectMapper();
        Student stuObj = null;
        try {
            stuObj = objectMapper.readValue(str, Student.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(stuObj.getName());
        System.out.println(stuObj.getPhone());
        System.out.println(stuObj.getStuId());
    }
}
