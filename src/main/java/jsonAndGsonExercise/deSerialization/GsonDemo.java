package jsonAndGsonExercise.deSerialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonDemo {
    public static void main(String[] args) {
        getJson(new Student("DK","DICE003",739891637,new Address("Street-04","Madhuban Calony","Pune","404441")));
        getJson(new Student("Ganesh","DICE001",863058402,new Address("Street-05","Jalgaon","Mumbai","233305")));
    }

    /**
     * Json conversion with help of Gson-API
     * @param student
     */
    private static void getJson(Student student) {
        //Gson gson=new Gson();
        Gson gson=new GsonBuilder().setPrettyPrinting().create();
        String response = gson.toJson(student);
        System.out.println("Response with help of Gson : "+response);
        //getObj(response);
    }

    /**
     * De-Serialization
     * @param str
     */
    private static void getObj(String str){
        Gson gson=new Gson();
        Student studentObj = gson.fromJson(str, Student.class);
        System.out.println("Response from ");
        System.out.println(studentObj.getName());
        System.out.println(studentObj.getPhone());
        System.out.println(studentObj.getStuId());
        System.out.println(studentObj.getAddress());
    }
}
