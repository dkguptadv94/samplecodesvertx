package jsonAndGsonExercise;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Iterator;

public class JsonDemo2 {
    public static void main(String[] args) {
        String inputJson="{\n" +
                "    \"result\": \"OK\",\n" +
                "    \"data\": {\n" +
                "        \"body\": {\n" +
                "            \"pdpHeader\": {\n" +
                "                \"hotelId\": \"424023\",\n" +
                "                \"destinationId\": \"1506246\",\n" +
                "                \"pointOfSaleId\": \"HCOM_US\",\n" +
                "                \"currencyCode\": \"USD\",\n" +
                "                \"occupancyKey\": \"A2\",\n" +
                "                \"hotelLocation\": {\n" +
                "                    \"coordinates\": {\n" +
                "                        \"latitude\": 40.75211,\n" +
                "                        \"longitude\": -73.98546\n" +
                "                    },\n" +
                "                    \"resolvedLocation\": \"CITY:1506246:PROVIDED:PROVIDED\",\n" +
                "                    \"locationName\": \"New York\"\n" +
                "                }\n" +
                "            },\n" +
                "            \"overview\": {\n" +
                "                \"overviewSections\": [\n" +
                "                    {\n" +
                "                        \"title\": \"Main amenities\",\n" +
                "                        \"type\": \"HOTEL_FEATURE\",\n" +
                "                        \"content\": [\n" +
                "                            \"Daily housekeeping\",\n" +
                "                            \"Restaurant and 2 bars/lounges\",\n" +
                "                            \"Rooftop terrace\",\n" +
                "                            \"Breakfast available\",\n" +
                "                            \"Fitness center\",\n" +
                "                            \"Self parking\",\n" +
                "                            \"24-hour front desk\",\n" +
                "                            \"Air conditioning\",\n" +
                "                            \"Front desk safe\",\n" +
                "                            \"ATM/banking services\",\n" +
                "                            \"Laundry service\",\n" +
                "                            \"Multilingual staff\",\n" +
                "                            \"Free WiFi \"\n" +
                "                        ],\n" +
                "                        \"contentType\": \"LIST\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"title\": \"For families\",\n" +
                "                        \"type\": \"FAMILY_FRIENDLY_SECTION\",\n" +
                "                        \"content\": [\n" +
                "                            \"Free cribs/infant beds\",\n" +
                "                            \"Connecting/adjoining rooms available\",\n" +
                "                            \"Private bathroom\",\n" +
                "                            \"Premium TV channels\",\n" +
                "                            \"Television\",\n" +
                "                            \"Free toiletries\"\n" +
                "                        ],\n" +
                "                        \"contentType\": \"LIST\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"title\": \"What’s around\",\n" +
                "                        \"type\": \"LOCATION_SECTION\",\n" +
                "                        \"content\": [\n" +
                "                            \"In Manhattan\",\n" +
                "                            \"5th Avenue - 2 min walk\",\n" +
                "                            \"Bryant Park - 4 min walk\",\n" +
                "                            \"Times Square - 5 min walk\",\n" +
                "                            \"Empire State Building - 8 min walk\",\n" +
                "                            \"Broadway - 9 min walk\",\n" +
                "                            \"Grand Central Terminal - 11 min walk\",\n" +
                "                            \"Madison Square Garden - 12 min walk\",\n" +
                "                            \"Radio City Music Hall - 13 min walk\",\n" +
                "                            \"Rockefeller Center - 15 min walk\",\n" +
                "                            \"Columbus Circle - 24 min walk\"\n" +
                "                        ],\n" +
                "                        \"contentType\": \"LIST\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"type\": \"TAGLINE\",\n" +
                "                        \"content\": [\n" +
                "                            \"<b>Luxury hotel with 2 bars/lounges, near Times Square </b>\"\n" +
                "                        ],\n" +
                "                        \"contentType\": \"LIST\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"type\": \"HOTEL_FREEBIES\",\n" +
                "                        \"content\": [\n" +
                "                            \"Free WiFi\"\n" +
                "                        ],\n" +
                "                        \"contentType\": \"LIST\"\n" +
                "                    }\n" +
                "                ]\n" +
                "            },\n" +
                "            \"hotelWelcomeRewards\": {\n" +
                "                \"applies\": true,\n" +
                "                \"info\": \"You can collect Hotels.com® Rewards stamps here\"\n" +
                "            },\n" +
                "            \"propertyDescription\": {\n" +
                "                \"clientToken\": \"Es626BNAVcX_QZkxp8P5_j3LzpE.\",\n" +
                "                \"address\": {\n" +
                "                    \"countryName\": \"United States of America\",\n" +
                "                    \"addressLine2\": \"New York\",\n" +
                "                    \"cityName\": \"New York\",\n" +
                "                    \"postalCode\": \"10018\",\n" +
                "                    \"provinceName\": \"NY\",\n" +
                "                    \"addressLine1\": \"63 W 38th St\",\n" +
                "                    \"countryCode\": \"USA\",\n" +
                "                    \"pattern\": \"AddressLine1,#AddressLine2,#CityName,#ProvinceName,#PostalCode,#CountryName\",\n" +
                "                    \"fullAddress\": \"63 W 38th St, New York, New York, NY, 10018, United States of America\"\n" +
                "                },\n" +
                "                \"priceMatchEnabled\": false,\n" +
                "                \"name\": \"Refinery Hotel\",\n" +
                "                \"starRatingTitle\": \"5 stars\",\n" +
                "                \"starRating\": 5.0,\n" +
                "                \"featuredPrice\": {\n" +
                "                    \"beforePriceText\": \"Lowest price\",\n" +
                "                    \"afterPriceText\": \"\",\n" +
                "                    \"pricingAvailability\": \"available on 10/16/22\",\n" +
                "                    \"pricingTooltip\": \"Price may be available on other dates\",\n" +
                "                    \"currentPrice\": {\n" +
                "                        \"formatted\": \"$350\",\n" +
                "                        \"plain\": 350.0\n" +
                "                    },\n" +
                "                    \"oldPrice\": \"$389\",\n" +
                "                    \"taxInclusiveFormatting\": false,\n" +
                "                    \"bookNowButton\": false\n" +
                "                },\n" +
                "                \"mapWidget\": {\n" +
                "                    \"staticMapUrl\": \"https://maps-api-ssl.google.com/maps/api/staticmap?center=40.75211,-73.98546&format=jpg&sensor=false&key=AIzaSyDaDqDNrxWrxcURixO2l6TbtV68X0Klf4U&zoom=16&size=834x443&scale&signature&signature=UjM9rOt8CZzHgf7zaz_C_Aj7__w=\"\n" +
                "                },\n" +
                "                \"roomTypeNames\": [\n" +
                "                    \"Studio, 1 Queen Bed\",\n" +
                "                    \"Studio, 1 King Bed\",\n" +
                "                    \"Deluxe Room, 2 Queen Beds\",\n" +
                "                    \"Atelier Executive Suite\"\n" +
                "                ],\n" +
                "                \"tagline\": [\n" +
                "                    \"<b>Luxury hotel with 2 bars/lounges, near Times Square </b>\"\n" +
                "                ],\n" +
                "                \"freebies\": [\n" +
                "                    \"Free WiFi\"\n" +
                "                ]\n" +
                "            },\n" +
                "            \"guestReviews\": {\n" +
                "                \"brands\": {\n" +
                "                    \"scale\": 10.0,\n" +
                "                    \"formattedScale\": \"10\",\n" +
                "                    \"rating\": 9.0,\n" +
                "                    \"formattedRating\": \"9.0\",\n" +
                "                    \"lowRating\": false,\n" +
                "                    \"badgeText\": \"Superb\",\n" +
                "                    \"total\": 1509\n" +
                "                }\n" +
                "            },\n" +
                "            \"atAGlance\": {\n" +
                "                \"keyFacts\": {\n" +
                "                    \"hotelSize\": [\n" +
                "                        \"This hotel has 197 rooms\",\n" +
                "                        \"This hotel is arranged over 12 floors\"\n" +
                "                    ],\n" +
                "                    \"arrivingLeaving\": [\n" +
                "                        \"Check-in time 3 PM-1 AM\",\n" +
                "                        \"Check-out time is  noon\",\n" +
                "                        \"Express check-in/out\"\n" +
                "                    ],\n" +
                "                    \"specialCheckInInstructions\": [\n" +
                "                        \"Front desk staff will greet guests on arrival. \"\n" +
                "                    ],\n" +
                "                    \"requiredAtCheckIn\": [\n" +
                "                        \"Credit card, debit card, or cash deposit required for incidental charges\",\n" +
                "                        \"Government-issued photo ID may be required\",\n" +
                "                        \"Minimum check-in age is 21\"\n" +
                "                    ]\n" +
                "                },\n" +
                "                \"travellingOrInternet\": {\n" +
                "                    \"travelling\": {\n" +
                "                        \"children\": [],\n" +
                "                        \"pets\": [\n" +
                "                            \"Pets allowed*\",\n" +
                "                            \"Service animals welcome\",\n" +
                "                            \"Food and water bowls available\"\n" +
                "                        ],\n" +
                "                        \"extraPeople\": []\n" +
                "                    },\n" +
                "                    \"internet\": [\n" +
                "                        \"Free WiFi in public areas\",\n" +
                "                        \"Free WiFi in rooms\"\n" +
                "                    ]\n" +
                "                },\n" +
                "                \"transportAndOther\": {\n" +
                "                    \"transport\": {\n" +
                "                        \"transfers\": [],\n" +
                "                        \"parking\": [\n" +
                "                            \"Secured onsite self parking (USD 72 per day)\"\n" +
                "                        ],\n" +
                "                        \"offsiteTransfer\": []\n" +
                "                    },\n" +
                "                    \"otherInformation\": [\n" +
                "                        \"Smoke-free property\"\n" +
                "                    ],\n" +
                "                    \"otherInclusions\": []\n" +
                "                }\n" +
                "            },\n" +
                "            \"amenities\": [\n" +
                "                {\n" +
                "                    \"heading\": \"In the hotel\",\n" +
                "                    \"listItems\": [\n" +
                "                        {\n" +
                "                            \"heading\": \"Food and drink\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"Full breakfast daily (surcharge)\",\n" +
                "                                \"2 bars/lounges\",\n" +
                "                                \"Restaurant\",\n" +
                "                                \"Coffee shop/cafe\",\n" +
                "                                \"Room service (during limited hours)\"\n" +
                "                            ]\n" +
                "                        },\n" +
                "                        {\n" +
                "                            \"heading\": \"Things to do\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"Fitness facilities\"\n" +
                "                            ]\n" +
                "                        },\n" +
                "                        {\n" +
                "                            \"heading\": \"Services\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"24-hour front desk\",\n" +
                "                                \"Concierge services\",\n" +
                "                                \"Tours/ticket assistance\",\n" +
                "                                \"Dry cleaning/laundry service\",\n" +
                "                                \"Free newspapers in lobby\",\n" +
                "                                \"Luggage storage\",\n" +
                "                                \"Multilingual staff\",\n" +
                "                                \"Porter/bellhop\"\n" +
                "                            ]\n" +
                "                        },\n" +
                "                        {\n" +
                "                            \"heading\": \"Facilities\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"Number of buildings/towers -  1\",\n" +
                "                                \"Year Built -  1912\",\n" +
                "                                \"Elevator\",\n" +
                "                                \"ATM/banking\",\n" +
                "                                \"Safe-deposit box at front desk\",\n" +
                "                                \"Rooftop terrace\"\n" +
                "                            ]\n" +
                "                        },\n" +
                "                        {\n" +
                "                            \"heading\": \"Accessibility\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"Braille or raised signage\",\n" +
                "                                \"Accessible bathroom\",\n" +
                "                                \"In-room accessibility\",\n" +
                "                                \"Roll-in shower\"\n" +
                "                            ]\n" +
                "                        },\n" +
                "                        {\n" +
                "                            \"heading\": \"Languages Spoken\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"Arabic\",\n" +
                "                                \"English\",\n" +
                "                                \"French\",\n" +
                "                                \"Italian\",\n" +
                "                                \"Polish\",\n" +
                "                                \"Portuguese\",\n" +
                "                                \"Russian\",\n" +
                "                                \"Spanish\"\n" +
                "                            ]\n" +
                "                        }\n" +
                "                    ]\n" +
                "                },\n" +
                "                {\n" +
                "                    \"heading\": \"In the room\",\n" +
                "                    \"listItems\": [\n" +
                "                        {\n" +
                "                            \"heading\": \"Home comforts\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"Air conditioning\",\n" +
                "                                \"Minibar\",\n" +
                "                                \"Bathrobes\",\n" +
                "                                \"Iron/ironing board\"\n" +
                "                            ]\n" +
                "                        },\n" +
                "                        {\n" +
                "                            \"heading\": \"Sleep well\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"Hypo-allergenic bedding available\",\n" +
                "                                \"Down comforter\",\n" +
                "                                \"Soundproofed rooms\",\n" +
                "                                \"Premium bedding\"\n" +
                "                            ]\n" +
                "                        },\n" +
                "                        {\n" +
                "                            \"heading\": \"Things to enjoy\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"In-room massage available\"\n" +
                "                            ]\n" +
                "                        },\n" +
                "                        {\n" +
                "                            \"heading\": \"Freshen up\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"Private bathroom\",\n" +
                "                                \"Designer toiletries\",\n" +
                "                                \"Hair dryer\"\n" +
                "                            ]\n" +
                "                        },\n" +
                "                        {\n" +
                "                            \"heading\": \"Be entertained\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"42-inch LCD TV\",\n" +
                "                                \"Pay movies\",\n" +
                "                                \"Premium TV channels\",\n" +
                "                                \"IPod docking station\"\n" +
                "                            ]\n" +
                "                        },\n" +
                "                        {\n" +
                "                            \"heading\": \"Stay connected\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"Desk\",\n" +
                "                                \"Free newspaper\",\n" +
                "                                \"Free WiFi\",\n" +
                "                                \"Phone\"\n" +
                "                            ]\n" +
                "                        },\n" +
                "                        {\n" +
                "                            \"heading\": \"Food and drink\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"Refrigerator (on request)\"\n" +
                "                            ]\n" +
                "                        },\n" +
                "                        {\n" +
                "                            \"heading\": \"More\",\n" +
                "                            \"listItems\": [\n" +
                "                                \"Daily housekeeping\",\n" +
                "                                \"In-room safe (laptop compatible)\",\n" +
                "                                \"Connecting/adjoining rooms available\"\n" +
                "                            ]\n" +
                "                        }\n" +
                "                    ]\n" +
                "                }\n" +
                "            ],\n" +
                "            \"hygieneAndCleanliness\": {\n" +
                "                \"title\": \"COVID-19: Hygiene and cleanliness\",\n" +
                "                \"hygieneQualifications\": {\n" +
                "                    \"title\": \"Official standards\",\n" +
                "                    \"qualifications\": [\n" +
                "                        \"This property advises that it adheres to SafeStay (AHLA - USA) cleaning and disinfection practices. \"\n" +
                "                    ]\n" +
                "                },\n" +
                "                \"healthAndSafetyMeasures\": {\n" +
                "                    \"title\": \"Enhanced health and safety measures\",\n" +
                "                    \"description\": \"This property advises that enhanced cleaning and guest safety measures are currently in place.\",\n" +
                "                    \"measures\": [\n" +
                "                        \"Property is cleaned with disinfectant\",\n" +
                "                        \"Staff wears personal protective equipment\",\n" +
                "                        \"Property confirms they are implementing enhanced cleaning measures\",\n" +
                "                        \"Shield between guests and staff in main contact areas\",\n" +
                "                        \"Social distancing measures are in place\",\n" +
                "                        \"Contactless check-in is available\",\n" +
                "                        \"Protective clothing is available to guests\",\n" +
                "                        \"Masks are available to guests\",\n" +
                "                        \"Gloves are available to guests\",\n" +
                "                        \"Individually-wrapped food options are available\",\n" +
                "                        \"Guests are provided with free hand sanitizer\"\n" +
                "                    ]\n" +
                "                }\n" +
                "            },\n" +
                "            \"smallPrint\": {\n" +
                "                \"alternativeNames\": [\n" +
                "                    \"Hotel Refinery\",\n" +
                "                    \"Refinery Hotel\",\n" +
                "                    \"Refinery Hotel New York\",\n" +
                "                    \"Refinery New York\",\n" +
                "                    \"Refinery\",\n" +
                "                    \"Refinery Hotel Hotel\",\n" +
                "                    \"Refinery Hotel New York\",\n" +
                "                    \"Refinery Hotel Hotel New York\"\n" +
                "                ],\n" +
                "                \"mandatoryFees\": [\n" +
                "                    \"<ul><li>Resort fee: USD 45.90 per accommodation, per night</li></ul><br/>\\n<p>Resort fee inclusions: </p><ul><li>Health club access</li><li>Fitness center access</li><li>Internet access (may be limited)</li></ul>\"\n" +
                "                ],\n" +
                "                \"optionalExtras\": [\n" +
                "                    \"<p><strong>Secured self parking</strong> costs USD 72 per day</p>\",\n" +
                "                    \"<p><strong>Rollaway</strong> beds are available for USD 50.0 per night</p>\",\n" +
                "                    \"<p><strong>Breakfast</strong> costs between USD 20 and USD 30 per person (approximately)</p>\",\n" +
                "                    \"<p><strong>Pets</strong> are allowed for an extra charge of USD 150 per pet, per stay</p>Service animals exempt from fees\"\n" +
                "                ],\n" +
                "                \"policies\": [\n" +
                "                    \"<p>The property has connecting/adjoining rooms, which are subject to availability and can be requested by contacting the property using the number on the booking confirmation. </p><p>This property advises that enhanced cleaning and guest safety measures are currently in place.</p><p>Disinfectant is used to clean the property; commonly-touched surfaces are cleaned with disinfectant between stays; bed sheets and towels are laundered at a temperature of at least 60°C/140°F.</p><p>Personal protective equipment, including masks and gloves, will be available to guests.</p><p>Social distancing measures are in place; staff at the property wear personal protective equipment; a shield is in place between staff and guests in main contact areas; periodic temperature checks are conducted on staff; guests are provided with hand sanitizer.</p><p>Contactless check-in and contactless check-out are available.</p><p>Individually-wrapped food options are available for breakfast, lunch, and dinner, and also through room service.</p><p>This property affirms that it adheres to the cleaning and disinfection practices of SafeStay (AHLA - USA).</p>\",\n" +
                "                    \"<p>This property accepts credit cards. Cash is not accepted.</p>\"\n" +
                "                ],\n" +
                "                \"mandatoryTaxesOrFees\": true,\n" +
                "                \"display\": true\n" +
                "            },\n" +
                "            \"specialFeatures\": {\n" +
                "                \"sections\": [\n" +
                "                    {\n" +
                "                        \"heading\": \"Dining\",\n" +
                "                        \"freeText\": \"<strong>Parker and Quinn</strong> - This restaurant specializes in American cuisine and serves breakfast, lunch, and dinner. Guests can enjoy drinks at the bar. <br/>\\n<p></p><strong>Winnies</strong> - Onsite lobby lounge. Open daily. <br/>\\n<p></p><strong>Rooftop Bar</strong> - This rooftop bar serves light fare only. Guests can enjoy alfresco dining (weather permitting). Open daily. \",\n" +
                "                        \"listItems\": [],\n" +
                "                        \"subsections\": []\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"heading\": \"Awards and affiliations\",\n" +
                "                        \"freeText\": \"The property is a member of the Small Luxury Hotels of the World. \",\n" +
                "                        \"listItems\": [],\n" +
                "                        \"subsections\": []\n" +
                "                    }\n" +
                "                ]\n" +
                "            },\n" +
                "            \"miscellaneous\": {\n" +
                "                \"pimmsAttributes\": \"DoubleStamps|MESOTESTUS|D13|TESCO\",\n" +
                "                \"showLegalInfoForStrikethroughPrices\": true,\n" +
                "                \"legalInfoForStrikethroughPrices\": \"The struck-out price is based on the property’s standard rate on our app, as determined and supplied by the property.\"\n" +
                "            },\n" +
                "            \"pageInfo\": {\n" +
                "                \"pageType\": \"dateless\",\n" +
                "                \"errors\": [],\n" +
                "                \"errorKeys\": []\n" +
                "            },\n" +
                "            \"hotelBadge\": {\n" +
                "                \"type\": \"vipBasic\",\n" +
                "                \"label\": \"VIP\",\n" +
                "                \"tooltipTitle\": \"VIP properties\",\n" +
                "                \"tooltipText\": \"VIP properties have been recognized for offering exceptional service to our customers.\"\n" +
                "            },\n" +
                "            \"unavailable\": {}\n" +
                "        },\n" +
                "        \"common\": {\n" +
                "            \"pointOfSale\": {\n" +
                "                \"numberSeparators\": \",.\",\n" +
                "                \"brandName\": \"Hotels.com\"\n" +
                "            },\n" +
                "            \"tracking\": {\n" +
                "                \"omniture\": {\n" +
                "                    \"s.prop34\": \"2022.06.949\",\n" +
                "                    \"s.eVar69\": \"Mob :: aApp\",\n" +
                "                    \"s.currencyCode\": \"USD\",\n" +
                "                    \"s.eVar26\": \"US:LA:NEWORLEANS:622\",\n" +
                "                    \"s.eVar16\": \"Mob :: aApp\",\n" +
                "                    \"s.products\": \"LOCAL;5558022\",\n" +
                "                    \"s.eVar29\": \"Mob :: aApp\",\n" +
                "                    \"s.eVar40\": \"D=v0\",\n" +
                "                    \"s.eVar95\": \"Unknown\",\n" +
                "                    \"s.eVar41\": \"USD\",\n" +
                "                    \"s.eVar31\": \"D=v0\",\n" +
                "                    \"s.eVar4\": \"|SI:anonymous|VS:returnVisitor|HCR:notApplicable|FC:notApplicable|NS:unknown|TI:notApplicable|SM:notApplicable|IR:anonymous|\",\n" +
                "                    \"s.eVar43\": \"en_US|HCOM_US|www.hotels.com\",\n" +
                "                    \"s.eVar32\": \"D=v0\",\n" +
                "                    \"s.eVar34\": \"H1068:005.001,H1871:007.002,M4869:001.000,M4953:001.000,M5342:000.000,M5756:001.000,M5758:001.000,M5759:001.000,M5760:001.000,M6271:001.000,M6439:000.000,M6553:000.000,M6946:000.000,M7296:000.000,M7305:000.000,M7308:000.000,M7353:000.000,M7362.0,M7635:000.000,M7844:000.000,M7983:000.000,M7990:000.000,M8033:000.000,M8248:000.000,M8336:000.000,M8400:000.000,M9035:000.000,M9056:000.000,M9167:000.000,M9168:000.000,M9469:000.000,M9475:000.000,M9493:000.000,M9629:000.000,M9733:007.001,M9810:000.000,M9890:000.000,M9924:000.000,M9930:000.000,M9974:000.000,M10013:002.001,M10025:000.000,M10049:000.000,M10057:000.000,M10101:000.000,M10268:000.000,M10291:000.000,M10335:000.000,M10473:000.000,M10517:000.000,M9215:001.000\",\n" +
                "                    \"s.eVar13\": \"389367\",\n" +
                "                    \"s.server\": \"www.hotels.com\",\n" +
                "                    \"s.prop28\": \"0\",\n" +
                "                    \"s.prop27\": \"15469e0f-5866-4e0b-bbf3-eb5ec2f83fd8\",\n" +
                "                    \"s.prop5\": \"389367\",\n" +
                "                    \"s.eVar80\": \"\",\n" +
                "                    \"s.prop48\": \"hotel details without dates description tab\",\n" +
                "                    \"s.prop36\": \"|SI:anonymous|VS:returnVisitor|HCR:notApplicable|FC:notApplicable|NS:unknown|TI:notApplicable|SM:notApplicable|IR:anonymous|\",\n" +
                "                    \"s.eVar93\": \"aws.us-west-2.unknown\"\n" +
                "                },\n" +
                "                \"pageViewBeaconUrl\": \"/taps/v1/PageView?deviceType=App-Phone&rooms=1&destRegionID=1506246&hotelIds=&adults=2&userAgent=PDE.4.1.440&userGuid=15469e0f-5866-4e0b-bbf3-eb5ec2f83fd8&pageName=Hotel-IS&slots=HIS_Car_A,HIS_Car_B,HIS_Car_C,HIS_Car_D,HIS_Car_E,HIS_Car_F,HIS_Car_G,HIS_Car_H,HIS_Car_I,HIS_Car_J&children=0&culture=en_US&testVersionOverride=4200.0,7983.0,7561.0,7990.0,8434.0,9493.0,10473.0&domain=www.hotels.com&action=pageview&publisher=expedia&userIP=127.0.0.1&programId=1\"\n" +
                "            }\n" +
                "        }\n" +
                "    },\n" +
                "    \"transportation\": {\n" +
                "        \"transportLocations\": [\n" +
                "            {\n" +
                "                \"category\": \"airport\",\n" +
                "                \"locations\": [\n" +
                "                    {\n" +
                "                        \"name\": \"LaGuardia Airport (LGA) -\",\n" +
                "                        \"distance\": \"\",\n" +
                "                        \"distanceInTime\": \"26 min drive\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"name\": \"John F. Kennedy Intl. Airport (JFK) -\",\n" +
                "                        \"distance\": \"\",\n" +
                "                        \"distanceInTime\": \"44 min drive\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"name\": \"Newark Liberty Intl. Airport (EWR) -\",\n" +
                "                        \"distance\": \"\",\n" +
                "                        \"distanceInTime\": \"36 min drive\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"name\": \"New York, NY (NYS-Skyports Seaplane Base) -\",\n" +
                "                        \"distance\": \"\",\n" +
                "                        \"distanceInTime\": \"3 min drive\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"name\": \"Teterboro, NJ (TEB) -\",\n" +
                "                        \"distance\": \"\",\n" +
                "                        \"distanceInTime\": \"15 min drive\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"name\": \"Linden, NJ (LDJ) -\",\n" +
                "                        \"distance\": \"\",\n" +
                "                        \"distanceInTime\": \"26 min drive\"\n" +
                "                    }\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"category\": \"train-station\",\n" +
                "                \"locations\": [\n" +
                "                    {\n" +
                "                        \"name\": \"New York W 32nd St. Station -\",\n" +
                "                        \"distance\": \"\",\n" +
                "                        \"distanceInTime\": \"7 min walk\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"name\": \"Grand Central - 42 St. Station -\",\n" +
                "                        \"distance\": \"\",\n" +
                "                        \"distanceInTime\": \"11 min walk\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"name\": \"Penn Station -\",\n" +
                "                        \"distance\": \"\",\n" +
                "                        \"distanceInTime\": \"13 min walk\"\n" +
                "                    }\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"category\": \"metro\",\n" +
                "                \"locations\": [\n" +
                "                    {\n" +
                "                        \"name\": \"42 St. - Bryant Pk. Station -\",\n" +
                "                        \"distance\": \"\",\n" +
                "                        \"distanceInTime\": \"3 min walk\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"name\": \"34 St. Station (Herald Square) -\",\n" +
                "                        \"distance\": \"\",\n" +
                "                        \"distanceInTime\": \"5 min walk\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"name\": \"Times Sq. - 42 St. Station -\",\n" +
                "                        \"distance\": \"\",\n" +
                "                        \"distanceInTime\": \"6 min walk\"\n" +
                "                    }\n" +
                "                ]\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    \"neighborhood\": {\n" +
                "        \"neighborhoodName\": \"Manhattan\"\n" +
                "    }\n" +
                "}";

        JSONObject jsonObjectInput=new JSONObject(inputJson);
        System.out.println("*** JsonResponse ***");
        getKey(jsonObjectInput,"hotelId");
    }
    public static void getKey(JSONObject jsonObject,String key){
        boolean exists = jsonObject.has(key);
        Iterator<?>keys;
        String nextKeys;
        if(!exists){
            keys = jsonObject.keys();
            while (keys.hasNext()){
                nextKeys = (String) keys.next();
                try{
                    if (jsonObject.get(nextKeys) instanceof JSONObject){
                        if(exists==false){
                            getKey(jsonObject.getJSONObject(nextKeys),key);
                        }
                    }else if(jsonObject.get(nextKeys)instanceof JSONArray){
                        JSONArray jsonArray = jsonObject.getJSONArray(nextKeys);
                            for (int i=0;i<jsonArray.length();i++){
                                String jsonArrayString = jsonArray.get(i).toString();
                                JSONObject innerJson=new JSONObject(jsonArrayString);
                                if (exists==false){
                                    getKey(innerJson,key);
                                }
                            }

                    }//end else-if

                }catch(Exception e){

                }//catch end
            }//end while
        }else {
            parseObject(jsonObject,key);
        }
    }
    public static void parseObject(JSONObject jsonObject,String key){
        //System.out.println(jsonObject.has(key));
        System.out.println(jsonObject.get(key));
    }
}
