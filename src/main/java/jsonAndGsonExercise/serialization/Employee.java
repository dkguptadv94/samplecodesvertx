package jsonAndGsonExercise.serialization;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Employee {
    private String name;
    private String position;
    private long salary;
    private double age;
    private Skills skills;
}
