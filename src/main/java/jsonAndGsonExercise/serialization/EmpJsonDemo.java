package jsonAndGsonExercise.serialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;

public class EmpJsonDemo {
    public static void main(String[] args) {
        getJsonResp(new Employee("Sourbh","SDE",2000,24,new Skills(Arrays.asList("Java","Angular"),"XP","Tech-Backend")));
        getJsonResp(new Employee("DK","SDE",1000,25,new Skills(Arrays.asList("Java","Python"),"Unix","MultiSkills")));
    }
    private static void getJsonResp(Employee employee) {
        Gson gson=new GsonBuilder().setPrettyPrinting().create();
        String convertedJson = gson.toJson(employee);
        System.out.println(convertedJson);
        getObjRes(convertedJson);
    }
    private static void getObjRes(String str) {
        Gson gson=new Gson();
        Employee employee = gson.fromJson(str, Employee.class);
        System.out.println(employee.getName());
        System.out.println(employee.getPosition());
        System.out.println(employee.getAge());
        System.out.println(employee.getSalary());
        System.out.println(employee.getSkills());
    }
}
