package jsonAndGsonExercise.serialization;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Skills {
    private List<String> programming;
    private String scripting;
    private String additional;
}
