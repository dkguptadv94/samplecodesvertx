package controller;

import data.*;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.Router;

public enum HotelsRouter {
    INSTANCE;
//new changes
    public Router router(Vertx vertx){
        Router router = Router.router(vertx);

        router.put("/search").handler(CitySearchImpl.INSTANCE::citySearchDto);
        router.get("/cityMetaData").handler(CityMetaDataImpl.INSTANCE::cityMetaDataDto);
        router.post("/proListRes").handler(ProListImpl.INSTANCE::proListCall);
        router.post("/proGetDetailsRes").handler(ProGetDetailsImpl.INSTANCE::proGetDetailsImpl);
        router.post("/proGetHotelPicRes").handler(ProGetHotelPicImpl.INSTANCE::proGetHotelPicImpl);
        return router;
    }
}
