package data;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.web.RoutingContext;
import models.properties.proGetDetails.proDetailsResponse.ProDetailsPojo;
import models.properties.proGetHotelPic.Image;
import models.properties.proGetHotelPic.ProGetHotelPicResponse;
import models.properties.proGetHotelPic.RoomImage;
import models.properties.proList.propSearchResponse.RoomDetails;
import utils.ResponseWriter;

import java.util.ArrayList;
import java.util.List;

public enum ProGetHotelPicImpl {
    INSTANCE;

    public void proGetHotelPicImpl(RoutingContext routingContext) {
        JsonObject bodyAsJson = routingContext.getBodyAsJson();
        ProGetHotelPicResponse proGetHotelPicBeans = proGetHotelPicResponse(bodyAsJson);

        ProDetailsPojo proDetailsPojo=new ProDetailsPojo();
        proDetailsPojo.setHotelId(proGetHotelPicBeans.getHotelId().toString());
        List<RoomDetails> roomDetails=new ArrayList<>();
        RoomDetails roomDetails1=new RoomDetails();
        for (RoomImage roomImage : proGetHotelPicBeans.getRoomImages()) {
            roomDetails1.setRoomId(roomImage.getRoomId().toString());
            for (Image image : roomImage.getImages()) {
                roomDetails1.setImage(image.getBaseUrl());
            }

        }
        roomDetails.add(roomDetails1);
        proDetailsPojo.setRooms(roomDetails);
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,proDetailsPojo);
    }

    public ProGetHotelPicResponse proGetHotelPicResponse(JsonObject request){
        try{
            String hotelId = request.getString("hotelId");
            Unirest.setTimeouts(0, 0);
            HttpResponse<String> response = Unirest.get(String.format("https://hotels4.p.rapidapi.com/properties/get-hotel-photos?id=%s",hotelId))
                    .header("X-RapidAPI-Key", "b17f058ad9msh91a954a8dc0795fp194fd0jsn28dcf001eee2")
                    .header("X-RapidAPI-Host", "hotels4.p.rapidapi.com")
                    .asString();

            if(response.getStatus()==200){
                return new Gson().fromJson(response.getBody(),ProGetHotelPicResponse.class);
            }
            return new ProGetHotelPicResponse();
        }catch(Exception e){
            e.printStackTrace();
        }
        return new ProGetHotelPicResponse();
    }
}
