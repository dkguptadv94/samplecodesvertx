package data;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.web.RoutingContext;
import models.cityMetaData.CityMetaDataResponse;
import utils.ResponseWriter;

public enum CityMetaDataImpl {
    INSTANCE;
    public void cityMetaDataDto(RoutingContext routingContext){
        JsonObject bodyAsJson = routingContext.getBodyAsJson();

        CityMetaDataResponse cityMetaDataBean = cityMetaData(bodyAsJson);
        String hcomLocale = cityMetaDataBean.hcomLocale;
        String name = cityMetaDataBean.name;
        String posName = cityMetaDataBean.posName;
        String accuWeatherLocale = cityMetaDataBean.accuWeatherLocale;
        if (cityMetaDataBean!=null){
            System.out.println("IF not null cityMetaData :  "+hcomLocale+name+posName+accuWeatherLocale);
        }
        System.out.println("MetaDataBean : "+ hcomLocale);
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,cityMetaDataBean);
    }
    public CityMetaDataResponse cityMetaData(JsonObject city){
        try{
            HttpResponse<String> response = Unirest.get("https://hotels4.p.rapidapi.com/get-meta-data"+city)
                    .header("X-RapidAPI-Key", "b17f058ad9msh91a954a8dc0795fp194fd0jsn28dcf001eee2")
                    .header("X-RapidAPI-Host", "hotels4.p.rapidapi.com")
                    .asString();
            if (response.getStatus()==200){
                return new Gson().fromJson(response.getBody(),CityMetaDataResponse.class);
                //return new JsonObject(response.getBody()).;
            }
            return new CityMetaDataResponse();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    return new CityMetaDataResponse();
    }
}
