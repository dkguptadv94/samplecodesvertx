package data;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.web.RoutingContext;
import models.properties.proGetDetails.*;
import models.properties.proGetDetails.proDetailsResponse.ProDetailsPojo;
import models.properties.proGetHotelPic.Image;
import models.properties.proGetHotelPic.ProGetHotelPicResponse;
import models.properties.proGetHotelPic.RoomImage;
import models.properties.proList.Coordinate;
import models.properties.proList.propSearchResponse.Amenities;
import models.properties.proList.propSearchResponse.RoomDetails;
import utils.ResponseWriter;

import java.util.ArrayList;
import java.util.List;

public enum ProGetDetailsImpl {
    INSTANCE;

    public void proGetDetailsImpl(RoutingContext routingContext) {
        JsonObject bodyAsJson = routingContext.getBodyAsJson();
        ProGetDetailsResponse proGetDetailsBeans = proGetDetailsResponse(bodyAsJson);
        ProGetHotelPicResponse roomBeans = ProGetHotelPicImpl.INSTANCE.proGetHotelPicResponse(bodyAsJson);
        ProDetailsPojo proDetailsPojo = new ProDetailsPojo();

        Body bodyPath = proGetDetailsBeans.getData().getBody();

        proDetailsPojo.setName(bodyPath.getPropertyDescription().getName());
        proDetailsPojo.setHotelId(bodyPath.getPdpHeader().getHotelId());
        proDetailsPojo.setDescription(bodyPath.getHygieneAndCleanliness().getHealthAndSafetyMeasures().description);
        proDetailsPojo.setCurrentPrice(bodyPath.getPropertyDescription().getFeaturedPrice().getCurrentPrice().getFormatted());
        proDetailsPojo.setUserRating(bodyPath.getGuestReviews().getBrands().getRating());
        proDetailsPojo.setStarRating(bodyPath.getPropertyDescription().getStarRating());

/**
 * RoomsDetails Properties...
 */
        List<RoomDetails> roomDetails=new ArrayList<>();
        RoomDetails roomDetails1=new RoomDetails();
        for (RoomImage roomImage : roomBeans.getRoomImages()) {
            roomDetails1.setRoomId(roomImage.getRoomId().toString());
            for (Image image : roomImage.getImages()) {
                roomDetails1.setImage(image.getBaseUrl());
            }
        }
        for (String roomTypeName : bodyPath.getPropertyDescription().getRoomTypeNames()) {
            roomDetails1.setDescription(roomTypeName);
        }
        for (OverviewSection overviewSection : bodyPath.getOverview().getOverviewSections()) {
            String title = overviewSection.getTitle();
            if (title!=null) {
                roomDetails1.setTitle(title.toUpperCase());
            }
        }
        roomDetails1.setPrice(bodyPath.getPropertyDescription().getFeaturedPrice().getCurrentPrice().getFormatted());

        roomDetails.add(roomDetails1);
        proDetailsPojo.setRooms(roomDetails);


/**
 * Amenities Properties
 */

        List<OverviewSection> overviewSections = bodyPath.getOverview().getOverviewSections();
        Amenities amenities=new Amenities();
        if (overviewSections!=null){
            for (OverviewSection overviewSection:overviewSections){
                for (String overviewContent : overviewSection.getContent()) {
                    if (overviewContent.toLowerCase().contains("wifi")){
                        amenities.setWifi(true);
                    }
                    else if (overviewContent.toLowerCase().contains("bar")) {
                        amenities.setBar(true);
                    } else if (overviewContent.toLowerCase().contains("food")) {
                        amenities.setFood(true);
                    } else if (overviewContent.toLowerCase().contains("pool")) {
                        amenities.setPool(true);
                    } else if (overviewContent.toLowerCase().contains("air")) {
                        amenities.setAc(true);
                    }
                }//end overviewSection
            }//end overviewSections
        }
        proDetailsPojo.setAmenities(amenities);


/**
 * Address Properties
 */

        Address address = new Address();
        address.setAddressLine1(bodyPath.getPropertyDescription().getAddress().getAddressLine1());
        address.setCityName(bodyPath.getPropertyDescription().getAddress().getCityName());
        address.setPostalCode(bodyPath.getPropertyDescription().getAddress().getPostalCode());
        address.setFullAddress(bodyPath.getPropertyDescription().getAddress().getFullAddress());
        proDetailsPojo.setAddress(address);
/**
 * Coordinate properties
 */

        Coordinate coordinates = new Coordinate();
        Coordinates coordinates1 = bodyPath.getPdpHeader().getHotelLocation().getCoordinates();
        coordinates.setLat(coordinates1.getLatitude());
        coordinates.setLon(coordinates1.getLongitude());
        proDetailsPojo.setCoordinate(coordinates);

        ResponseWriter.INSTANCE.writeJsonResponse(routingContext, proDetailsPojo);
    }
    /**
     *API calling
     * @param request
     * @return ProGetDetailsResponse.
     */
    public ProGetDetailsResponse proGetDetailsResponse(JsonObject request){
        try{
            String hotelId = request.getString("hotelId");
            HttpResponse<String> response = Unirest.get(String.format("https://hotels4.p.rapidapi.com/properties/get-details?id=%s",hotelId))
                    .header("X-RapidAPI-Key", "b17f058ad9msh91a954a8dc0795fp194fd0jsn28dcf001eee2")
                    .header("X-RapidAPI-Host", "hotels4.p.rapidapi.com")
                    .asString();

            if(response.getStatus()==200){
                return new Gson().fromJson(response.getBody(),ProGetDetailsResponse.class);
            }
            return  new ProGetDetailsResponse();
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ProGetDetailsResponse();
    }
}
