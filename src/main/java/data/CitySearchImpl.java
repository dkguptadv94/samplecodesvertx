package data;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.web.RoutingContext;
import models.CityData;
import models.MasterPojo;
import models.citySearch.CitySearchResponse;
import models.citySearch.Entity;
import models.citySearch.Suggestion;
import utils.ResponseWriter;

import java.util.ArrayList;
import java.util.List;

public enum CitySearchImpl {
    INSTANCE;

    public void citySearchDto(RoutingContext routingContext){
        MasterPojo masterPojo=new MasterPojo();
        JsonObject bodyAsJson = routingContext.getBodyAsJson();
        String ctSearch = bodyAsJson.getString("city");
        CitySearchResponse storeData = searchCity(ctSearch);
        List<Suggestion> suggestions1 = storeData.getSuggestions();
        for (Suggestion sData:suggestions1) {
            List<Entity> entities1 = sData.getEntities();
            List<CityData>listCity=new ArrayList<>();
            for (Entity eData:entities1) {
                CityData cityData=new CityData();

                cityData.setCaption(eData.getCaption());
                cityData.setType(eData.getType());
                cityData.setName(eData.getName());
                cityData.setDestinationId(eData.getDestinationId());

                listCity.add(cityData);
            }
            masterPojo.setListCity(listCity);
        }
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,masterPojo);
    }
    public CitySearchResponse searchCity(String city){
        try{
            HttpResponse<String> response = Unirest.get("https://hotels4.p.rapidapi.com/locations/v2/search?query="+city)
                    .header("X-RapidAPI-Key", "b17f058ad9msh91a954a8dc0795fp194fd0jsn28dcf001eee2")
                    .header("X-RapidAPI-Host", "hotels4.p.rapidapi.com")
                    .asString();
            if (response.getStatus()==200){
                return new Gson().fromJson(response.getBody(), CitySearchResponse.class);
            }
            return new CitySearchResponse();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return  new CitySearchResponse();
    }
}
