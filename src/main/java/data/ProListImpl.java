package data;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.web.RoutingContext;
import models.properties.proList.*;
import models.properties.proList.propSearchResponse.Amenities;
import models.properties.proList.propSearchResponse.MasterProSearchRes;
import models.properties.proList.propSearchResponse.Property;
import org.apache.commons.beanutils.BeanUtils;
import utils.ResponseWriter;
import java.util.ArrayList;
import java.util.List;

public enum ProListImpl {
    INSTANCE;

    public void proListCall (RoutingContext routingContext) {
        JsonObject bodyAsJson = routingContext.getBodyAsJson();
        ProListResponse proListResponse = proList(bodyAsJson);
        List<Property> properties = new ArrayList<>();

        for (Result result : proListResponse.getData().getBody().getSearchResults().getResults()) {
            Property property = new Property();
/**
 * If there is some common properties here so we can call this method place of set and get
 */
//            try{
//                BeanUtils.copyProperties(property,result);
//
//            }catch (Exception e){
//                e.printStackTrace();
//            }

            property.setPropertyId(result.getId().toString());
            property.setName(result.getName().toUpperCase());
            property.setRoomLeft(result.getRoomsLeft());
            property.setCoordinate(result.getCoordinate());
            property.setAddress(result.getAddress().getStreetAddress());
            property.setUserRating(result.getGuestReviews().getRating());
            property.setStarRating(result.getStarRating().longValue());
            property.setCurrentPrice(result.getRatePlan().getPrice().getExactCurrent());

            for (Option option : proListResponse.getData().getBody().getSortResults().getOptions()) {
                for (EnhancedChoice enhancedChoice : option.getEnhancedChoices()) {
                    property.setDescription(enhancedChoice.choices.toString());
                }
            }

            if (result.getOptimizedThumbUrls() != null){
                property.setThumbnailImage(result.getOptimizedThumbUrls().getSrpDesktop());
            }

            Filters filters = proListResponse.getData().getBody().getFilters();
            if (filters.getFacilities() != null) {
                Amenities amenities = new Amenities();

                for (Item item : filters.getFacilities().getItems()) {
                    if (item.getLabel().toLowerCase().contains("wifi")){
                        amenities.setWifi(true);
                    }else if (item.getLabel().toLowerCase().contains("pool")){
                        amenities.setPool(true);
                    } else if (item.getLabel().toLowerCase().contains("ac")){
                        amenities.setAc(true);
                    } else if (item.getLabel().toLowerCase().contains("food")){
                        amenities.setFood(true);
                    } else if (item.getLabel().toLowerCase().contains("bar")) {
                        amenities.setBar(true);
                    }
                }
                property.setAmenities(amenities);
            }

//            result.getName(); // property name
//            result.getId(); // property id
//            result.getThumbnailUrl(); // property thumbnail img
//            result.getRoomsLeft(); // property rooms left
//            result.getRatePlan(); // property price
//
//            result.getAddress(); // property address
//            result.getCoordinate(); // property coordinates
//            result.getBadging(); // property rating and all
//            result.getGuestReviews(); // property guest review

            properties.add(property);
        }
        MasterProSearchRes masterProSearchRes = new MasterProSearchRes();
        masterProSearchRes.setProperties(properties);

        ResponseWriter.INSTANCE.writeJsonResponse(routingContext, masterProSearchRes);
    }

    public ProListResponse proList(JsonObject request) {
        try {
            String checkInDate = request.getString("fromDate");
            String checkOutDate = request.getString("toDate");
            String destinationId = request.getString("destinationId");
            String adultCount = request.getString("adultCount");

            HttpResponse<String> response = Unirest.get(String.format("https://hotels4.p.rapidapi.com/properties/list?destinationId=%s&pageNumber=1&pageSize=10&checkIn=%s&checkOut=%s&sortOrder=PRICE&locale=en_US&currency=USD&adults=%s", destinationId, checkInDate, checkOutDate, adultCount))
                    .header("X-RapidAPI-Key", "b17f058ad9msh91a954a8dc0795fp194fd0jsn28dcf001eee2")
                    .header("X-RapidAPI-Host", "hotels4.p.rapidapi.com")
                    .asString();

            if (response.getStatus() == 200) {
                return new Gson().fromJson(response.getBody(), ProListResponse.class);
            }
            return new ProListResponse();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ProListResponse();
    }
}