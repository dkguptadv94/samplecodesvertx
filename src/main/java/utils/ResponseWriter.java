package utils;

import io.vertx.rxjava.ext.web.RoutingContext;

public enum ResponseWriter {
    INSTANCE;
    public void writeJsonResponse(RoutingContext routingContext,Object response){
        routingContext.response().putHeader("content-type","application/json")
                .setStatusCode(200).end(GsonMapper.INSTANCE.getMapper().toJson(response));
    }

    public void writeErrorJsonResponse(RoutingContext routingContext,ErrorResponse response){
        routingContext.response().putHeader("content-type","application/json")
                .setStatusCode(409).end(GsonMapper.INSTANCE.getMapper().toJson(response));
    }
}
