
package models.reviews.list1;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Review {

    @Expose
    private String formattedRating;
    @Expose
    private String postedOn;
    @Expose
    private String qualitativeBadgeText;
    @Expose
    private Long rating;
    @Expose
    private String recommendedBy;
    @Expose
    private String reviewType;
    @Expose
    private String summary;
    @Expose
    private String title;

    public String getFormattedRating() {
        return formattedRating;
    }

    public void setFormattedRating(String formattedRating) {
        this.formattedRating = formattedRating;
    }

    public String getPostedOn() {
        return postedOn;
    }

    public void setPostedOn(String postedOn) {
        this.postedOn = postedOn;
    }

    public String getQualitativeBadgeText() {
        return qualitativeBadgeText;
    }

    public void setQualitativeBadgeText(String qualitativeBadgeText) {
        this.qualitativeBadgeText = qualitativeBadgeText;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    public String getRecommendedBy() {
        return recommendedBy;
    }

    public void setRecommendedBy(String recommendedBy) {
        this.recommendedBy = recommendedBy;
    }

    public String getReviewType() {
        return reviewType;
    }

    public void setReviewType(String reviewType) {
        this.reviewType = reviewType;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
