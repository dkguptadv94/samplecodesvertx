
package models.reviews.list1;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class GuestReviewGroups {

    @Expose
    private GuestReviewOverview guestReviewOverview;
    @Expose
    private GuestReviewPagination guestReviewPagination;
    @Expose
    private List<GuestReview> guestReviews;

    public GuestReviewOverview getGuestReviewOverview() {
        return guestReviewOverview;
    }

    public void setGuestReviewOverview(GuestReviewOverview guestReviewOverview) {
        this.guestReviewOverview = guestReviewOverview;
    }

    public GuestReviewPagination getGuestReviewPagination() {
        return guestReviewPagination;
    }

    public void setGuestReviewPagination(GuestReviewPagination guestReviewPagination) {
        this.guestReviewPagination = guestReviewPagination;
    }

    public List<GuestReview> getGuestReviews() {
        return guestReviews;
    }

    public void setGuestReviews(List<GuestReview> guestReviews) {
        this.guestReviews = guestReviews;
    }

}
