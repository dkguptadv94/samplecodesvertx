
package models.reviews.list1;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Omniture {

    @Expose
    private String evar34;

    public String getEvar34() {
        return evar34;
    }

    public void setEvar34(String evar34) {
        this.evar34 = evar34;
    }

}
