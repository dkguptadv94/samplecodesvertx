
package models.reviews.list1;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OverallScoreBreakdown {

    @Expose
    private Long amount;
    @Expose
    private String formattedScore;
    @Expose
    private Long score;

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getFormattedScore() {
        return formattedScore;
    }

    public void setFormattedScore(String formattedScore) {
        this.formattedScore = formattedScore;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

}
