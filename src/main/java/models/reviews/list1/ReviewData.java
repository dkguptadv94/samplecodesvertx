
package models.reviews.list1;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ReviewData {

    @Expose
    private GuestReviewGroups guestReviewGroups;
    @Expose
    private String hotelId;
    @Expose
    private Omniture omniture;

    public GuestReviewGroups getGuestReviewGroups() {
        return guestReviewGroups;
    }

    public void setGuestReviewGroups(GuestReviewGroups guestReviewGroups) {
        this.guestReviewGroups = guestReviewGroups;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public Omniture getOmniture() {
        return omniture;
    }

    public void setOmniture(Omniture omniture) {
        this.omniture = omniture;
    }

}
