
package models.reviews.list1;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class GuestReviewPagination {

    @Expose
    private Boolean nextPage;
    @Expose
    private Long page;
    @Expose
    private Boolean previousPage;

    public Boolean getNextPage() {
        return nextPage;
    }

    public void setNextPage(Boolean nextPage) {
        this.nextPage = nextPage;
    }

    public Long getPage() {
        return page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public Boolean getPreviousPage() {
        return previousPage;
    }

    public void setPreviousPage(Boolean previousPage) {
        this.previousPage = previousPage;
    }

}
