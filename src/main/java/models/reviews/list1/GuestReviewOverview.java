
package models.reviews.list1;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class GuestReviewOverview {

    @Expose
    private Double cleanliness;
    @Expose
    private String formattedCleanliness;
    @Expose
    private String formattedHotelCondition;
    @Expose
    private String formattedHotelService;
    @Expose
    private String formattedOverall;
    @Expose
    private String formattedRoomComfort;
    @Expose
    private Double hotelCondition;
    @Expose
    private Long hotelService;
    @Expose
    private Long neighbourhood;
    @Expose
    private Double overall;
    @Expose
    private List<OverallScoreBreakdown> overallScoreBreakdown;
    @Expose
    private String qualitativeBadgeText;
    @Expose
    private Long roomComfort;
    @Expose
    private Long totalCount;

    public Double getCleanliness() {
        return cleanliness;
    }

    public void setCleanliness(Double cleanliness) {
        this.cleanliness = cleanliness;
    }

    public String getFormattedCleanliness() {
        return formattedCleanliness;
    }

    public void setFormattedCleanliness(String formattedCleanliness) {
        this.formattedCleanliness = formattedCleanliness;
    }

    public String getFormattedHotelCondition() {
        return formattedHotelCondition;
    }

    public void setFormattedHotelCondition(String formattedHotelCondition) {
        this.formattedHotelCondition = formattedHotelCondition;
    }

    public String getFormattedHotelService() {
        return formattedHotelService;
    }

    public void setFormattedHotelService(String formattedHotelService) {
        this.formattedHotelService = formattedHotelService;
    }

    public String getFormattedOverall() {
        return formattedOverall;
    }

    public void setFormattedOverall(String formattedOverall) {
        this.formattedOverall = formattedOverall;
    }

    public String getFormattedRoomComfort() {
        return formattedRoomComfort;
    }

    public void setFormattedRoomComfort(String formattedRoomComfort) {
        this.formattedRoomComfort = formattedRoomComfort;
    }

    public Double getHotelCondition() {
        return hotelCondition;
    }

    public void setHotelCondition(Double hotelCondition) {
        this.hotelCondition = hotelCondition;
    }

    public Long getHotelService() {
        return hotelService;
    }

    public void setHotelService(Long hotelService) {
        this.hotelService = hotelService;
    }

    public Long getNeighbourhood() {
        return neighbourhood;
    }

    public void setNeighbourhood(Long neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public Double getOverall() {
        return overall;
    }

    public void setOverall(Double overall) {
        this.overall = overall;
    }

    public List<OverallScoreBreakdown> getOverallScoreBreakdown() {
        return overallScoreBreakdown;
    }

    public void setOverallScoreBreakdown(List<OverallScoreBreakdown> overallScoreBreakdown) {
        this.overallScoreBreakdown = overallScoreBreakdown;
    }

    public String getQualitativeBadgeText() {
        return qualitativeBadgeText;
    }

    public void setQualitativeBadgeText(String qualitativeBadgeText) {
        this.qualitativeBadgeText = qualitativeBadgeText;
    }

    public Long getRoomComfort() {
        return roomComfort;
    }

    public void setRoomComfort(Long roomComfort) {
        this.roomComfort = roomComfort;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

}
