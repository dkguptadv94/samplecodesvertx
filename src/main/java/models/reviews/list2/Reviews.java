
package models.reviews.list2;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Reviews {

    @SerializedName("__typename")
    private String _Typename;
    @Expose
    private Body body;
    @Expose
    private Hermes hermes;

    public String get_Typename() {
        return _Typename;
    }

    public void set_Typename(String _Typename) {
        this._Typename = _Typename;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Hermes getHermes() {
        return hermes;
    }

    public void setHermes(Hermes hermes) {
        this.hermes = hermes;
    }

}
