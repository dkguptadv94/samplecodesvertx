
package models.reviews.list2;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Hermes {

    @SerializedName("__typename")
    private String _Typename;
    @Expose
    private List<Group> groups;

    public String get_Typename() {
        return _Typename;
    }

    public void set_Typename(String _Typename) {
        this._Typename = _Typename;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

}
