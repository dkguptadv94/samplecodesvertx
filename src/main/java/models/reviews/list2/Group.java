
package models.reviews.list2;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Group {

    @SerializedName("__typename")
    private String _Typename;
    @Expose
    private List<Item> items;
    @Expose
    private Object separatorText;

    public String get_Typename() {
        return _Typename;
    }

    public void set_Typename(String _Typename) {
        this._Typename = _Typename;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Object getSeparatorText() {
        return separatorText;
    }

    public void setSeparatorText(Object separatorText) {
        this.separatorText = separatorText;
    }

}
