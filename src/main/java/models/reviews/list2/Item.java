
package models.reviews.list2;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Item {

    @SerializedName("__typename")
    private String _Typename;
    @Expose
    private String badge;
    @Expose
    private String brand;
    @Expose
    private String description;
    @Expose
    private String genuineMsg;
    @Expose
    private Object googleTranslateEnabled;
    @Expose
    private String itineraryId;
    @Expose
    private String rating;
    @Expose
    private String reviewDate;
    @Expose
    private String reviewDbDate;
    @Expose
    private Object reviewSubmitDate;
    @Expose
    private Reviewer reviewer;
    @Expose
    private String summary;
    @Expose
    private String tripType;
    @Expose
    private String tripTypeText;

    public String get_Typename() {
        return _Typename;
    }

    public void set_Typename(String _Typename) {
        this._Typename = _Typename;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenuineMsg() {
        return genuineMsg;
    }

    public void setGenuineMsg(String genuineMsg) {
        this.genuineMsg = genuineMsg;
    }

    public Object getGoogleTranslateEnabled() {
        return googleTranslateEnabled;
    }

    public void setGoogleTranslateEnabled(Object googleTranslateEnabled) {
        this.googleTranslateEnabled = googleTranslateEnabled;
    }

    public String getItineraryId() {
        return itineraryId;
    }

    public void setItineraryId(String itineraryId) {
        this.itineraryId = itineraryId;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(String reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getReviewDbDate() {
        return reviewDbDate;
    }

    public void setReviewDbDate(String reviewDbDate) {
        this.reviewDbDate = reviewDbDate;
    }

    public Object getReviewSubmitDate() {
        return reviewSubmitDate;
    }

    public void setReviewSubmitDate(Object reviewSubmitDate) {
        this.reviewSubmitDate = reviewSubmitDate;
    }

    public Reviewer getReviewer() {
        return reviewer;
    }

    public void setReviewer(Reviewer reviewer) {
        this.reviewer = reviewer;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getTripTypeText() {
        return tripTypeText;
    }

    public void setTripTypeText(String tripTypeText) {
        this.tripTypeText = tripTypeText;
    }

}
