
package models.reviews.list2;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Overall {

    @SerializedName("__typename")
    private String _Typename;
    @Expose
    private String badgeText;
    @Expose
    private String rating;
    @Expose
    private RatingAspects ratingAspects;
    @Expose
    private List<Score> scores;
    @Expose
    private String selectedFilterType;
    @Expose
    private Object topRated;
    @Expose
    private Long total;
    @Expose
    private Object whatGuestsSay;

    public String get_Typename() {
        return _Typename;
    }

    public void set_Typename(String _Typename) {
        this._Typename = _Typename;
    }

    public String getBadgeText() {
        return badgeText;
    }

    public void setBadgeText(String badgeText) {
        this.badgeText = badgeText;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public RatingAspects getRatingAspects() {
        return ratingAspects;
    }

    public void setRatingAspects(RatingAspects ratingAspects) {
        this.ratingAspects = ratingAspects;
    }

    public List<Score> getScores() {
        return scores;
    }

    public void setScores(List<Score> scores) {
        this.scores = scores;
    }

    public String getSelectedFilterType() {
        return selectedFilterType;
    }

    public void setSelectedFilterType(String selectedFilterType) {
        this.selectedFilterType = selectedFilterType;
    }

    public Object getTopRated() {
        return topRated;
    }

    public void setTopRated(Object topRated) {
        this.topRated = topRated;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Object getWhatGuestsSay() {
        return whatGuestsSay;
    }

    public void setWhatGuestsSay(Object whatGuestsSay) {
        this.whatGuestsSay = whatGuestsSay;
    }

}
