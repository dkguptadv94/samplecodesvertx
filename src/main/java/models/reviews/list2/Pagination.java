
package models.reviews.list2;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Pagination {

    @SerializedName("__typename")
    private String _Typename;
    @Expose
    private Long currentPage;
    @Expose
    private Object nextURL;
    @Expose
    private Long totalPages;

    public String get_Typename() {
        return _Typename;
    }

    public void set_Typename(String _Typename) {
        this._Typename = _Typename;
    }

    public Long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    public Object getNextURL() {
        return nextURL;
    }

    public void setNextURL(Object nextURL) {
        this.nextURL = nextURL;
    }

    public Long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Long totalPages) {
        this.totalPages = totalPages;
    }

}
