
package models.reviews.list2;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Filter {

    @SerializedName("__typename")
    private String _Typename;
    @Expose
    private Long count;
    @Expose
    private String name;
    @Expose
    private String type;

    public String get_Typename() {
        return _Typename;
    }

    public void set_Typename(String _Typename) {
        this._Typename = _Typename;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
