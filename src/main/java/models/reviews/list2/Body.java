
package models.reviews.list2;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Body {

    @SerializedName("__typename")
    private String _Typename;
    @Expose
    private ReviewContent reviewContent;

    public String get_Typename() {
        return _Typename;
    }

    public void set_Typename(String _Typename) {
        this._Typename = _Typename;
    }

    public ReviewContent getReviewContent() {
        return reviewContent;
    }

    public void setReviewContent(ReviewContent reviewContent) {
        this.reviewContent = reviewContent;
    }

}
