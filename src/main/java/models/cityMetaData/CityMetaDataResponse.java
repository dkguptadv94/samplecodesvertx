
package models.cityMetaData;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CityMetaDataResponse {
    public String name;
    public String posName;
    public String hcomLocale;
    public String accuWeatherLocale;

}
