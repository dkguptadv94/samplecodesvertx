
package models.citySearch;

import java.util.List;
import javax.annotation.Generated;

import lombok.Data;

@Generated("net.hexar.json2pojo")
@Data
public class CitySearchResponse {
    private Object autoSuggestInstance;
    private Boolean geocodeFallback;
    private Boolean misspellingfallback;
    private Long moresuggestions;
    private List<Suggestion> suggestions;
    private String term;
    private String trackingID;

}
