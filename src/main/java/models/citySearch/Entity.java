
package models.citySearch;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
@Data
public class Entity {

    private String caption;
    private String destinationId;
    private String geoId;
    private Object landmarkCityDestinationId;
    private Double latitude;
    private Double longitude;
    private String name;
    private String redirectPage;
    private Object searchDetail;
    private String type;

}
