
package models.citySearch;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
@Data
public class Suggestion {

    @Expose
    private List<Entity> entities;
    @Expose
    private String group;

}
