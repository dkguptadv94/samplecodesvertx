
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
@lombok.Data
public class Data {

    @Expose
    private Body body;
    @Expose
    private Common common;
}
