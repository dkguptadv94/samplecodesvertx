
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class SmallPrint {

    @Expose
    private List<String> alternativeNames;
    @Expose
    private Boolean display;
    @Expose
    private List<Object> mandatoryFees;
    @Expose
    private Boolean mandatoryTaxesOrFees;
    @Expose
    private List<String> optionalExtras;
    @Expose
    private List<String> policies;

    public List<String> getAlternativeNames() {
        return alternativeNames;
    }

    public void setAlternativeNames(List<String> alternativeNames) {
        this.alternativeNames = alternativeNames;
    }

    public Boolean getDisplay() {
        return display;
    }

    public void setDisplay(Boolean display) {
        this.display = display;
    }

    public List<Object> getMandatoryFees() {
        return mandatoryFees;
    }

    public void setMandatoryFees(List<Object> mandatoryFees) {
        this.mandatoryFees = mandatoryFees;
    }

    public Boolean getMandatoryTaxesOrFees() {
        return mandatoryTaxesOrFees;
    }

    public void setMandatoryTaxesOrFees(Boolean mandatoryTaxesOrFees) {
        this.mandatoryTaxesOrFees = mandatoryTaxesOrFees;
    }

    public List<String> getOptionalExtras() {
        return optionalExtras;
    }

    public void setOptionalExtras(List<String> optionalExtras) {
        this.optionalExtras = optionalExtras;
    }

    public List<String> getPolicies() {
        return policies;
    }

    public void setPolicies(List<String> policies) {
        this.policies = policies;
    }

}
