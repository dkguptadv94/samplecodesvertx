
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class GuestReviews {

    @Expose
    private Brands brands;
    @Expose
    private TripAdvisor tripAdvisor;
    @Expose
    private List<TrustYouReview> trustYouReviews;

    public Brands getBrands() {
        return brands;
    }

    public void setBrands(Brands brands) {
        this.brands = brands;
    }

    public TripAdvisor getTripAdvisor() {
        return tripAdvisor;
    }

    public void setTripAdvisor(TripAdvisor tripAdvisor) {
        this.tripAdvisor = tripAdvisor;
    }

    public List<TrustYouReview> getTrustYouReviews() {
        return trustYouReviews;
    }

    public void setTrustYouReviews(List<TrustYouReview> trustYouReviews) {
        this.trustYouReviews = trustYouReviews;
    }

}
