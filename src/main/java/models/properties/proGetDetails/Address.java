
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class Address {

    @Expose
    private String addressLine1;
    @Expose
    private String cityName;
    @Expose
    private String countryCode;
    @Expose
    private String countryName;
    @Expose
    private String fullAddress;
    @Expose
    private String pattern;
    @Expose
    private String postalCode;
    @Expose
    private String provinceName;

}
