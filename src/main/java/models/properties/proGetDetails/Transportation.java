
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Transportation {

    @Expose
    private List<TransportLocation> transportLocations;

    public List<TransportLocation> getTransportLocations() {
        return transportLocations;
    }

    public void setTransportLocations(List<TransportLocation> transportLocations) {
        this.transportLocations = transportLocations;
    }

}
