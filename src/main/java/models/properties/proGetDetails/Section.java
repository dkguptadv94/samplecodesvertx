
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Section {

    @Expose
    private String freeText;
    @Expose
    private String heading;
    @Expose
    private List<ListItem> listItems;
    @Expose
    private List<Object> subsections;

    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public List<ListItem> getListItems() {
        return listItems;
    }

    public void setListItems(List<ListItem> listItems) {
        this.listItems = listItems;
    }

    public List<Object> getSubsections() {
        return subsections;
    }

    public void setSubsections(List<Object> subsections) {
        this.subsections = subsections;
    }

}
