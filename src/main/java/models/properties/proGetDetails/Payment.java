
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Payment {

    @Expose
    private Book book;
    @Expose
    private Boolean noCCRequired;
    @Expose
    private String paymentMessage;

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Boolean getNoCCRequired() {
        return noCCRequired;
    }

    public void setNoCCRequired(Boolean noCCRequired) {
        this.noCCRequired = noCCRequired;
    }

    public String getPaymentMessage() {
        return paymentMessage;
    }

    public void setPaymentMessage(String paymentMessage) {
        this.paymentMessage = paymentMessage;
    }

}
