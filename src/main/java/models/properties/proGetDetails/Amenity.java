
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class Amenity {

    private String heading;
    private List<ListItem> listItems;

}
