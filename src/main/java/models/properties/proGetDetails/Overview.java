
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Overview {

    @Expose
    private List<OverviewSection> overviewSections;

    public List<OverviewSection> getOverviewSections() {
        return overviewSections;
    }

    public void setOverviewSections(List<OverviewSection> overviewSections) {
        this.overviewSections = overviewSections;
    }

}
