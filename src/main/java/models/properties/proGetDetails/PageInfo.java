
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PageInfo {

    @Expose
    private List<Object> errorKeys;
    @Expose
    private List<Object> errors;
    @Expose
    private String pageType;

    public List<Object> getErrorKeys() {
        return errorKeys;
    }

    public void setErrorKeys(List<Object> errorKeys) {
        this.errorKeys = errorKeys;
    }

    public List<Object> getErrors() {
        return errors;
    }

    public void setErrors(List<Object> errors) {
        this.errors = errors;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

}
