
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class RoomsAndRates {

    @Expose
    private String bookingUrl;
    @Expose
    private String priceColumnHeading;
    @Expose
    private Boolean ratePlanWithOffersExists;
    @Expose
    private List<Room> rooms;

    public String getBookingUrl() {
        return bookingUrl;
    }

    public void setBookingUrl(String bookingUrl) {
        this.bookingUrl = bookingUrl;
    }

    public String getPriceColumnHeading() {
        return priceColumnHeading;
    }

    public void setPriceColumnHeading(String priceColumnHeading) {
        this.priceColumnHeading = priceColumnHeading;
    }

    public Boolean getRatePlanWithOffersExists() {
        return ratePlanWithOffersExists;
    }

    public void setRatePlanWithOffersExists(Boolean ratePlanWithOffersExists) {
        this.ratePlanWithOffersExists = ratePlanWithOffersExists;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

}
