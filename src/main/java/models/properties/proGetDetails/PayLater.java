
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PayLater {

    @Expose
    private DisplayedPrice displayedPrice;
    @Expose
    private OverrideBookingParams overrideBookingParams;
    @Expose
    private String welcomeRewards;

    public DisplayedPrice getDisplayedPrice() {
        return displayedPrice;
    }

    public void setDisplayedPrice(DisplayedPrice displayedPrice) {
        this.displayedPrice = displayedPrice;
    }

    public OverrideBookingParams getOverrideBookingParams() {
        return overrideBookingParams;
    }

    public void setOverrideBookingParams(OverrideBookingParams overrideBookingParams) {
        this.overrideBookingParams = overrideBookingParams;
    }

    public String getWelcomeRewards() {
        return welcomeRewards;
    }

    public void setWelcomeRewards(String welcomeRewards) {
        this.welcomeRewards = welcomeRewards;
    }

}
