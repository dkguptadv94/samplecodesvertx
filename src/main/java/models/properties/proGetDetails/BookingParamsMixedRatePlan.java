
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class BookingParamsMixedRatePlan {

    @Expose
    private String bookingApiVersion;
    @Expose
    private String currency;
    @Expose
    private Boolean init;
    @Expose
    private String interstitial;
    @Expose
    private String marketingChannelCode;
    @Expose
    private String minPrice;
    @Expose
    private String numberOfRoomType;
    @Expose
    private List<OrderItem> orderItems;
    @Expose
    private String propertyDetailsDisplayRate;

    public String getBookingApiVersion() {
        return bookingApiVersion;
    }

    public void setBookingApiVersion(String bookingApiVersion) {
        this.bookingApiVersion = bookingApiVersion;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getInit() {
        return init;
    }

    public void setInit(Boolean init) {
        this.init = init;
    }

    public String getInterstitial() {
        return interstitial;
    }

    public void setInterstitial(String interstitial) {
        this.interstitial = interstitial;
    }

    public String getMarketingChannelCode() {
        return marketingChannelCode;
    }

    public void setMarketingChannelCode(String marketingChannelCode) {
        this.marketingChannelCode = marketingChannelCode;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public String getNumberOfRoomType() {
        return numberOfRoomType;
    }

    public void setNumberOfRoomType(String numberOfRoomType) {
        this.numberOfRoomType = numberOfRoomType;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public String getPropertyDetailsDisplayRate() {
        return propertyDetailsDisplayRate;
    }

    public void setPropertyDetailsDisplayRate(String propertyDetailsDisplayRate) {
        this.propertyDetailsDisplayRate = propertyDetailsDisplayRate;
    }

}
