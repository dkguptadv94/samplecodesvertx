
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Miscellaneous {

    @Expose
    private String pimmsAttributes;
    @Expose
    private Boolean showLegalInfoForStrikethroughPrices;

    public String getPimmsAttributes() {
        return pimmsAttributes;
    }

    public void setPimmsAttributes(String pimmsAttributes) {
        this.pimmsAttributes = pimmsAttributes;
    }

    public Boolean getShowLegalInfoForStrikethroughPrices() {
        return showLegalInfoForStrikethroughPrices;
    }

    public void setShowLegalInfoForStrikethroughPrices(Boolean showLegalInfoForStrikethroughPrices) {
        this.showLegalInfoForStrikethroughPrices = showLegalInfoForStrikethroughPrices;
    }

}
