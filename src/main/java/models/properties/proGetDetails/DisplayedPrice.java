
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DisplayedPrice {

    @Expose
    private Boolean approximated;
    @Expose
    private String displayedPrice;
    @Expose
    private String priceInfo;

    public Boolean getApproximated() {
        return approximated;
    }

    public void setApproximated(Boolean approximated) {
        this.approximated = approximated;
    }

    public String getDisplayedPrice() {
        return displayedPrice;
    }

    public void setDisplayedPrice(String displayedPrice) {
        this.displayedPrice = displayedPrice;
    }

    public String getPriceInfo() {
        return priceInfo;
    }

    public void setPriceInfo(String priceInfo) {
        this.priceInfo = priceInfo;
    }

}
