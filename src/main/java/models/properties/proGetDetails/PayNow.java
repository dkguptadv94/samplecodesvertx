
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PayNow {

    @Expose
    private DisplayedPrice displayedPrice;
    @Expose
    private String giftCard;
    @Expose
    private OverrideBookingParams overrideBookingParams;
    @Expose
    private Boolean payInCurrency;
    @Expose
    private String welcomeRewards;

    public DisplayedPrice getDisplayedPrice() {
        return displayedPrice;
    }

    public void setDisplayedPrice(DisplayedPrice displayedPrice) {
        this.displayedPrice = displayedPrice;
    }

    public String getGiftCard() {
        return giftCard;
    }

    public void setGiftCard(String giftCard) {
        this.giftCard = giftCard;
    }

    public OverrideBookingParams getOverrideBookingParams() {
        return overrideBookingParams;
    }

    public void setOverrideBookingParams(OverrideBookingParams overrideBookingParams) {
        this.overrideBookingParams = overrideBookingParams;
    }

    public Boolean getPayInCurrency() {
        return payInCurrency;
    }

    public void setPayInCurrency(Boolean payInCurrency) {
        this.payInCurrency = payInCurrency;
    }

    public String getWelcomeRewards() {
        return welcomeRewards;
    }

    public void setWelcomeRewards(String welcomeRewards) {
        this.welcomeRewards = welcomeRewards;
    }

}
