
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Tracking {

    @Expose
    private Omniture omniture;
    @Expose
    private String pageViewBeaconUrl;

    public Omniture getOmniture() {
        return omniture;
    }

    public void setOmniture(Omniture omniture) {
        this.omniture = omniture;
    }

    public String getPageViewBeaconUrl() {
        return pageViewBeaconUrl;
    }

    public void setPageViewBeaconUrl(String pageViewBeaconUrl) {
        this.pageViewBeaconUrl = pageViewBeaconUrl;
    }

}
