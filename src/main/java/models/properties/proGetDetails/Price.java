
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Price {

    @Expose
    private String current;
    @Expose
    private String info;
    @Expose
    private NightlyPriceBreakdown nightlyPriceBreakdown;
    @Expose
    private String old;
    @Expose
    private Long unformattedCurrent;

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public NightlyPriceBreakdown getNightlyPriceBreakdown() {
        return nightlyPriceBreakdown;
    }

    public void setNightlyPriceBreakdown(NightlyPriceBreakdown nightlyPriceBreakdown) {
        this.nightlyPriceBreakdown = nightlyPriceBreakdown;
    }

    public String getOld() {
        return old;
    }

    public void setOld(String old) {
        this.old = old;
    }

    public Long getUnformattedCurrent() {
        return unformattedCurrent;
    }

    public void setUnformattedCurrent(Long unformattedCurrent) {
        this.unformattedCurrent = unformattedCurrent;
    }

}
