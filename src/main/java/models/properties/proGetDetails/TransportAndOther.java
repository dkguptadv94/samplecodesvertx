
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class TransportAndOther {

    @Expose
    private List<Object> otherInclusions;
    @Expose
    private List<String> otherInformation;
    @Expose
    private Transport transport;

    public List<Object> getOtherInclusions() {
        return otherInclusions;
    }

    public void setOtherInclusions(List<Object> otherInclusions) {
        this.otherInclusions = otherInclusions;
    }

    public List<String> getOtherInformation() {
        return otherInformation;
    }

    public void setOtherInformation(List<String> otherInformation) {
        this.otherInformation = otherInformation;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

}
