
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Transport {

    @Expose
    private List<Object> offsiteTransfer;
    @Expose
    private List<String> parking;
    @Expose
    private List<Object> transfers;

    public List<Object> getOffsiteTransfer() {
        return offsiteTransfer;
    }

    public void setOffsiteTransfer(List<Object> offsiteTransfer) {
        this.offsiteTransfer = offsiteTransfer;
    }

    public List<String> getParking() {
        return parking;
    }

    public void setParking(List<String> parking) {
        this.parking = parking;
    }

    public List<Object> getTransfers() {
        return transfers;
    }

    public void setTransfers(List<Object> transfers) {
        this.transfers = transfers;
    }

}
