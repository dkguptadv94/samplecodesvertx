package models.properties.proGetDetails;

import lombok.Data;

import java.util.ArrayList;

@Data
public class HealthAndSafetyMeasures {
    public String title;
    public String description;
    public ArrayList<String> measures;
}
