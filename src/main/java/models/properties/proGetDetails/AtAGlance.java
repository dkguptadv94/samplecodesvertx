
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AtAGlance {

    @Expose
    private KeyFacts keyFacts;
    @Expose
    private TransportAndOther transportAndOther;
    @Expose
    private TravellingOrInternet travellingOrInternet;

    public KeyFacts getKeyFacts() {
        return keyFacts;
    }

    public void setKeyFacts(KeyFacts keyFacts) {
        this.keyFacts = keyFacts;
    }

    public TransportAndOther getTransportAndOther() {
        return transportAndOther;
    }

    public void setTransportAndOther(TransportAndOther transportAndOther) {
        this.transportAndOther = transportAndOther;
    }

    public TravellingOrInternet getTravellingOrInternet() {
        return travellingOrInternet;
    }

    public void setTravellingOrInternet(TravellingOrInternet travellingOrInternet) {
        this.travellingOrInternet = travellingOrInternet;
    }

}
