
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Brands {

    @Expose
    private String badgeText;
    @Expose
    private String formattedRating;
    @Expose
    private String formattedScale;
    @Expose
    private Boolean lowRating;
    @Expose
    private Double rating;
    @Expose
    private Long scale;
    @Expose
    private Long total;

    public String getBadgeText() {
        return badgeText;
    }

    public void setBadgeText(String badgeText) {
        this.badgeText = badgeText;
    }

    public String getFormattedRating() {
        return formattedRating;
    }

    public void setFormattedRating(String formattedRating) {
        this.formattedRating = formattedRating;
    }

    public String getFormattedScale() {
        return formattedScale;
    }

    public void setFormattedScale(String formattedScale) {
        this.formattedScale = formattedScale;
    }

    public Boolean getLowRating() {
        return lowRating;
    }

    public void setLowRating(Boolean lowRating) {
        this.lowRating = lowRating;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Long getScale() {
        return scale;
    }

    public void setScale(Long scale) {
        this.scale = scale;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
