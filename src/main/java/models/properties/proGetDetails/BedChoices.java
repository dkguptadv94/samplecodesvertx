
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class BedChoices {

    @Expose
    private List<String> extraBedTypes;
    @Expose
    private List<String> mainOptions;

    public List<String> getExtraBedTypes() {
        return extraBedTypes;
    }

    public void setExtraBedTypes(List<String> extraBedTypes) {
        this.extraBedTypes = extraBedTypes;
    }

    public List<String> getMainOptions() {
        return mainOptions;
    }

    public void setMainOptions(List<String> mainOptions) {
        this.mainOptions = mainOptions;
    }

}
