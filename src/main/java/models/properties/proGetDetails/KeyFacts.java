
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class KeyFacts {

    @Expose
    private List<String> arrivingLeaving;
    @Expose
    private List<String> hotelSize;
    @Expose
    private List<String> requiredAtCheckIn;
    @Expose
    private List<Object> specialCheckInInstructions;

    public List<String> getArrivingLeaving() {
        return arrivingLeaving;
    }

    public void setArrivingLeaving(List<String> arrivingLeaving) {
        this.arrivingLeaving = arrivingLeaving;
    }

    public List<String> getHotelSize() {
        return hotelSize;
    }

    public void setHotelSize(List<String> hotelSize) {
        this.hotelSize = hotelSize;
    }

    public List<String> getRequiredAtCheckIn() {
        return requiredAtCheckIn;
    }

    public void setRequiredAtCheckIn(List<String> requiredAtCheckIn) {
        this.requiredAtCheckIn = requiredAtCheckIn;
    }

    public List<Object> getSpecialCheckInInstructions() {
        return specialCheckInInstructions;
    }

    public void setSpecialCheckInInstructions(List<Object> specialCheckInInstructions) {
        this.specialCheckInInstructions = specialCheckInInstructions;
    }

}
