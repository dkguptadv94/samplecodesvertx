
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Room {

    @Expose
    private AdditionalInfo additionalInfo;
    @Expose
    private BedChoices bedChoices;
    @Expose
    private List<Image> images;
    @Expose
    private MaxOccupancy maxOccupancy;
    @Expose
    private String name;
    @Expose
    private List<RatePlan> ratePlans;

    public AdditionalInfo getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(AdditionalInfo additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public BedChoices getBedChoices() {
        return bedChoices;
    }

    public void setBedChoices(BedChoices bedChoices) {
        this.bedChoices = bedChoices;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public MaxOccupancy getMaxOccupancy() {
        return maxOccupancy;
    }

    public void setMaxOccupancy(MaxOccupancy maxOccupancy) {
        this.maxOccupancy = maxOccupancy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RatePlan> getRatePlans() {
        return ratePlans;
    }

    public void setRatePlans(List<RatePlan> ratePlans) {
        this.ratePlans = ratePlans;
    }

}
