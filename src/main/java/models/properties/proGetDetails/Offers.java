
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Offers {

    @Expose
    private Offer offer;
    @Expose
    private List<Object> valueAdds;

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public List<Object> getValueAdds() {
        return valueAdds;
    }

    public void setValueAdds(List<Object> valueAdds) {
        this.valueAdds = valueAdds;
    }

}
