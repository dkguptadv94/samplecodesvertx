
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class PdpHeader {

    @Expose
    private String currencyCode;
    @Expose
    private String destinationId;
    @Expose
    private String hotelId;
    @Expose
    private HotelLocation hotelLocation;
    @Expose
    private String occupancyKey;
    @Expose
    private String pointOfSaleId;

}
