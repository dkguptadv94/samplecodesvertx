
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class MaxOccupancy {

    @Expose
    private Long children;
    @Expose
    private String messageChildren;
    @Expose
    private String messageTotal;
    @Expose
    private Long total;

    public Long getChildren() {
        return children;
    }

    public void setChildren(Long children) {
        this.children = children;
    }

    public String getMessageChildren() {
        return messageChildren;
    }

    public void setMessageChildren(String messageChildren) {
        this.messageChildren = messageChildren;
    }

    public String getMessageTotal() {
        return messageTotal;
    }

    public void setMessageTotal(String messageTotal) {
        this.messageTotal = messageTotal;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
