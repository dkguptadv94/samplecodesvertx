
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Book {

    @Expose
    private BookingParamsMixedRatePlan bookingParamsMixedRatePlan;
    @Expose
    private String caption;
    @Expose
    private PaymentPreference paymentPreference;

    public BookingParamsMixedRatePlan getBookingParamsMixedRatePlan() {
        return bookingParamsMixedRatePlan;
    }

    public void setBookingParamsMixedRatePlan(BookingParamsMixedRatePlan bookingParamsMixedRatePlan) {
        this.bookingParamsMixedRatePlan = bookingParamsMixedRatePlan;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public PaymentPreference getPaymentPreference() {
        return paymentPreference;
    }

    public void setPaymentPreference(PaymentPreference paymentPreference) {
        this.paymentPreference = paymentPreference;
    }

}
