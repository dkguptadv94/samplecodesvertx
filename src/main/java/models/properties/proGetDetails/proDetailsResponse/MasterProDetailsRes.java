package models.properties.proGetDetails.proDetailsResponse;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MasterProDetailsRes {
    private List<ProDetailsPojo> properties=new ArrayList<>();
}
