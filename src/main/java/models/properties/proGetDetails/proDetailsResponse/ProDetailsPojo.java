package models.properties.proGetDetails.proDetailsResponse;

import lombok.Data;
import models.properties.proGetDetails.Address;
import models.properties.proList.Coordinate;
import models.properties.proList.propSearchResponse.Amenities;
import models.properties.proList.propSearchResponse.Images;
import models.properties.proList.propSearchResponse.RoomDetails;

import java.util.List;

@Data
public class ProDetailsPojo {
    private String name;
    private String hotelId;
    private Long starRating;
    private String description;
    private Coordinate coordinate;
    private Address address;
    private Double userRating;
    private String currentPrice;
    private Amenities amenities;

    private Long roomLeft;
    private String thumbnailImage;



    private List<RoomDetails> rooms;
    private List<Images> images;

}
