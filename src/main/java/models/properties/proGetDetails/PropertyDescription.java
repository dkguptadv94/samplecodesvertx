
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class PropertyDescription {

    @Expose
    private Address address;
    @Expose
    private String clientToken;
    @Expose
    private FeaturedPrice featuredPrice;
    @Expose
    private List<String> freebies;
    @Expose
    private MapWidget mapWidget;
    @Expose
    private String name;
    @Expose
    private Boolean priceMatchEnabled;
    @Expose
    private List<String> roomTypeNames;
    @Expose
    private Long starRating;
    @Expose
    private String starRatingTitle;
    @Expose
    private List<String> tagline;

}
