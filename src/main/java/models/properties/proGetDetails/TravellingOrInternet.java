
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class TravellingOrInternet {

    @Expose
    private List<String> internet;
    @Expose
    private Travelling travelling;

    public List<String> getInternet() {
        return internet;
    }

    public void setInternet(List<String> internet) {
        this.internet = internet;
    }

    public Travelling getTravelling() {
        return travelling;
    }

    public void setTravelling(Travelling travelling) {
        this.travelling = travelling;
    }

}
