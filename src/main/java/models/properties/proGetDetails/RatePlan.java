
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class RatePlan {

    private Cancellation cancellation;
    private List<Cancellation> cancellations;
    private List<Feature> features;
    private Occupancy occupancy;
    private Offers offers;
    private Payment payment;
    private Price price;
    private WelcomeRewards welcomeRewards;

}
