
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Options {

    @Expose
    private PayLater payLater;
    @Expose
    private PayNow payNow;

    public PayLater getPayLater() {
        return payLater;
    }

    public void setPayLater(PayLater payLater) {
        this.payLater = payLater;
    }

    public PayNow getPayNow() {
        return payNow;
    }

    public void setPayNow(PayNow payNow) {
        this.payNow = payNow;
    }

}
