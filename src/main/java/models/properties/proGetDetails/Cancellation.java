
package models.properties.proGetDetails;

import lombok.Data;

@Data
public class Cancellation {

    private String additionalInfo;
    private String cancellationDate;
    private Boolean free;
    private String info;
    private String period;
    private String title;

}
