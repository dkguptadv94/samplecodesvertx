
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Feature {

    @Expose
    private String dataSourceInfo;
    @Expose
    private String featureType;
    @Expose
    private String info;
    @Expose
    private String title;

    public String getDataSourceInfo() {
        return dataSourceInfo;
    }

    public void setDataSourceInfo(String dataSourceInfo) {
        this.dataSourceInfo = dataSourceInfo;
    }

    public String getFeatureType() {
        return featureType;
    }

    public void setFeatureType(String featureType) {
        this.featureType = featureType;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
