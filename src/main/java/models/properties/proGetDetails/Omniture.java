
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Omniture {

    @SerializedName("s.currencyCode")
    private String sCurrencyCode;
    @SerializedName("s.eVar13")
    private String sEVar13;
    @SerializedName("s.eVar16")
    private String sEVar16;
    @SerializedName("s.eVar29")
    private String sEVar29;
    @SerializedName("s.eVar31")
    private String sEVar31;
    @SerializedName("s.eVar32")
    private String sEVar32;
    @SerializedName("s.eVar34")
    private String sEVar34;
    @SerializedName("s.eVar4")
    private String sEVar4;
    @SerializedName("s.eVar40")
    private String sEVar40;
    @SerializedName("s.eVar41")
    private String sEVar41;
    @SerializedName("s.eVar43")
    private String sEVar43;
    @SerializedName("s.eVar69")
    private String sEVar69;
    @SerializedName("s.eVar80")
    private String sEVar80;
    @SerializedName("s.eVar93")
    private String sEVar93;
    @SerializedName("s.eVar95")
    private String sEVar95;
    @SerializedName("s.products")
    private String sProducts;
    @SerializedName("s.prop27")
    private String sProp27;
    @SerializedName("s.prop28")
    private String sProp28;
    @SerializedName("s.prop34")
    private String sProp34;
    @SerializedName("s.prop36")
    private String sProp36;
    @SerializedName("s.prop48")
    private String sProp48;
    @SerializedName("s.prop5")
    private String sProp5;
    @SerializedName("s.server")
    private String sServer;

    public String getSCurrencyCode() {
        return sCurrencyCode;
    }

    public void setSCurrencyCode(String sCurrencyCode) {
        this.sCurrencyCode = sCurrencyCode;
    }

    public String getSEVar13() {
        return sEVar13;
    }

    public void setSEVar13(String sEVar13) {
        this.sEVar13 = sEVar13;
    }

    public String getSEVar16() {
        return sEVar16;
    }

    public void setSEVar16(String sEVar16) {
        this.sEVar16 = sEVar16;
    }

    public String getSEVar29() {
        return sEVar29;
    }

    public void setSEVar29(String sEVar29) {
        this.sEVar29 = sEVar29;
    }

    public String getSEVar31() {
        return sEVar31;
    }

    public void setSEVar31(String sEVar31) {
        this.sEVar31 = sEVar31;
    }

    public String getSEVar32() {
        return sEVar32;
    }

    public void setSEVar32(String sEVar32) {
        this.sEVar32 = sEVar32;
    }

    public String getSEVar34() {
        return sEVar34;
    }

    public void setSEVar34(String sEVar34) {
        this.sEVar34 = sEVar34;
    }

    public String getSEVar4() {
        return sEVar4;
    }

    public void setSEVar4(String sEVar4) {
        this.sEVar4 = sEVar4;
    }

    public String getSEVar40() {
        return sEVar40;
    }

    public void setSEVar40(String sEVar40) {
        this.sEVar40 = sEVar40;
    }

    public String getSEVar41() {
        return sEVar41;
    }

    public void setSEVar41(String sEVar41) {
        this.sEVar41 = sEVar41;
    }

    public String getSEVar43() {
        return sEVar43;
    }

    public void setSEVar43(String sEVar43) {
        this.sEVar43 = sEVar43;
    }

    public String getSEVar69() {
        return sEVar69;
    }

    public void setSEVar69(String sEVar69) {
        this.sEVar69 = sEVar69;
    }

    public String getSEVar80() {
        return sEVar80;
    }

    public void setSEVar80(String sEVar80) {
        this.sEVar80 = sEVar80;
    }

    public String getSEVar93() {
        return sEVar93;
    }

    public void setSEVar93(String sEVar93) {
        this.sEVar93 = sEVar93;
    }

    public String getSEVar95() {
        return sEVar95;
    }

    public void setSEVar95(String sEVar95) {
        this.sEVar95 = sEVar95;
    }

    public String getSProducts() {
        return sProducts;
    }

    public void setSProducts(String sProducts) {
        this.sProducts = sProducts;
    }

    public String getSProp27() {
        return sProp27;
    }

    public void setSProp27(String sProp27) {
        this.sProp27 = sProp27;
    }

    public String getSProp28() {
        return sProp28;
    }

    public void setSProp28(String sProp28) {
        this.sProp28 = sProp28;
    }

    public String getSProp34() {
        return sProp34;
    }

    public void setSProp34(String sProp34) {
        this.sProp34 = sProp34;
    }

    public String getSProp36() {
        return sProp36;
    }

    public void setSProp36(String sProp36) {
        this.sProp36 = sProp36;
    }

    public String getSProp48() {
        return sProp48;
    }

    public void setSProp48(String sProp48) {
        this.sProp48 = sProp48;
    }

    public String getSProp5() {
        return sProp5;
    }

    public void setSProp5(String sProp5) {
        this.sProp5 = sProp5;
    }

    public String getSServer() {
        return sServer;
    }

    public void setSServer(String sServer) {
        this.sServer = sServer;
    }

}
