
package models.properties.proGetDetails;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ListItem {

    private String heading;
    @JsonProperty("listItems")
    private List<String> services;

}
