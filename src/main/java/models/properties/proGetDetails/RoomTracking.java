
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class RoomTracking {

    @Expose
    private String prop49;
    @Expose
    private String prop71;
    @Expose
    private String prop75;

    public String getProp49() {
        return prop49;
    }

    public void setProp49(String prop49) {
        this.prop49 = prop49;
    }

    public String getProp71() {
        return prop71;
    }

    public void setProp71(String prop71) {
        this.prop71 = prop71;
    }

    public String getProp75() {
        return prop75;
    }

    public void setProp75(String prop75) {
        this.prop75 = prop75;
    }

}
