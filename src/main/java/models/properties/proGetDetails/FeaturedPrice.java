
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class FeaturedPrice {

    @Expose
    private Boolean bookNowButton;
    @Expose
    private CurrentPrice currentPrice;
    @Expose
    private Offer offer;
    @Expose
    private String oldPrice;
    @Expose
    private String priceInfo;
    @Expose
    private String pricingTooltip;
    @Expose
    private Boolean taxInclusiveFormatting;

    public Boolean getBookNowButton() {
        return bookNowButton;
    }

    public void setBookNowButton(Boolean bookNowButton) {
        this.bookNowButton = bookNowButton;
    }

    public CurrentPrice getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(CurrentPrice currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getPriceInfo() {
        return priceInfo;
    }

    public void setPriceInfo(String priceInfo) {
        this.priceInfo = priceInfo;
    }

    public String getPricingTooltip() {
        return pricingTooltip;
    }

    public void setPricingTooltip(String pricingTooltip) {
        this.pricingTooltip = pricingTooltip;
    }

    public Boolean getTaxInclusiveFormatting() {
        return taxInclusiveFormatting;
    }

    public void setTaxInclusiveFormatting(Boolean taxInclusiveFormatting) {
        this.taxInclusiveFormatting = taxInclusiveFormatting;
    }

}
