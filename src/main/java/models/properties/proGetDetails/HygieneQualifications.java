package models.properties.proGetDetails;

import lombok.Data;

import java.util.ArrayList;

@Data
public class HygieneQualifications {
    public String title;
    public ArrayList<String> qualifications;
}
