
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OrderItem {

    @Expose
    private String arrivalDate;
    @Expose
    private String businessModel;
    @Expose
    private String departureDate;
    @Expose
    private String destinationId;
    @Expose
    private String hotelCityId;
    @Expose
    private String hotelId;
    @Expose
    private String rateCode;
    @Expose
    private String ratePlanConfiguration;
    @Expose
    private String roomTypeCode;
    @Expose
    private String sequenceNumber;
    @Expose
    private String tripId;
    @Expose
    private Long tspid;

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getBusinessModel() {
        return businessModel;
    }

    public void setBusinessModel(String businessModel) {
        this.businessModel = businessModel;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getHotelCityId() {
        return hotelCityId;
    }

    public void setHotelCityId(String hotelCityId) {
        this.hotelCityId = hotelCityId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getRateCode() {
        return rateCode;
    }

    public void setRateCode(String rateCode) {
        this.rateCode = rateCode;
    }

    public String getRatePlanConfiguration() {
        return ratePlanConfiguration;
    }

    public void setRatePlanConfiguration(String ratePlanConfiguration) {
        this.ratePlanConfiguration = ratePlanConfiguration;
    }

    public String getRoomTypeCode() {
        return roomTypeCode;
    }

    public void setRoomTypeCode(String roomTypeCode) {
        this.roomTypeCode = roomTypeCode;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public Long getTspid() {
        return tspid;
    }

    public void setTspid(Long tspid) {
        this.tspid = tspid;
    }

}
