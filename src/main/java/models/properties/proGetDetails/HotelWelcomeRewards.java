
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class HotelWelcomeRewards {

    @Expose
    private Boolean applies;
    @Expose
    private String info;

    public Boolean getApplies() {
        return applies;
    }

    public void setApplies(Boolean applies) {
        this.applies = applies;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

}
