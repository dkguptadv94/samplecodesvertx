
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CurrentPrice {

    @Expose
    private String formatted;
    @Expose
    private Long plain;

    public String getFormatted() {
        return formatted;
    }

    public void setFormatted(String formatted) {
        this.formatted = formatted;
    }

    public Long getPlain() {
        return plain;
    }

    public void setPlain(Long plain) {
        this.plain = plain;
    }

}
