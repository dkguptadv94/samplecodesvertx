
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class OverviewSection {

    private List<String> content;
    private String contentType;
    private String title;
    private String type;
}
