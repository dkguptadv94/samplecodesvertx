
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class NightlyPriceBreakdown {

    @Expose
    private List<AdditionalColumn> additionalColumns;
    @Expose
    private List<Object> nightlyPrices;

    public List<AdditionalColumn> getAdditionalColumns() {
        return additionalColumns;
    }

    public void setAdditionalColumns(List<AdditionalColumn> additionalColumns) {
        this.additionalColumns = additionalColumns;
    }

    public List<Object> getNightlyPrices() {
        return nightlyPrices;
    }

    public void setNightlyPrices(List<Object> nightlyPrices) {
        this.nightlyPrices = nightlyPrices;
    }

}
