
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PointOfSale {

    @Expose
    private String brandName;
    @Expose
    private String numberSeparators;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getNumberSeparators() {
        return numberSeparators;
    }

    public void setNumberSeparators(String numberSeparators) {
        this.numberSeparators = numberSeparators;
    }

}
