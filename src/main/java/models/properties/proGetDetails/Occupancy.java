
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Occupancy {

    @Expose
    private Long maxAdults;
    @Expose
    private Long maxChildren;

    public Long getMaxAdults() {
        return maxAdults;
    }

    public void setMaxAdults(Long maxAdults) {
        this.maxAdults = maxAdults;
    }

    public Long getMaxChildren() {
        return maxChildren;
    }

    public void setMaxChildren(Long maxChildren) {
        this.maxChildren = maxChildren;
    }

}
