
package models.properties.proGetDetails;

import lombok.Data;

import javax.annotation.Generated;

@Data
public class HygieneAndCleanliness {
    public String title;
    public HygieneQualifications hygieneQualifications;
    public HealthAndSafetyMeasures healthAndSafetyMeasures;
}
