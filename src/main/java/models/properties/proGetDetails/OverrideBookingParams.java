
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OverrideBookingParams {

    @Expose
    private String businessModel;
    @Expose
    private String displayedCurrentPrice;

    public String getBusinessModel() {
        return businessModel;
    }

    public void setBusinessModel(String businessModel) {
        this.businessModel = businessModel;
    }

    public String getDisplayedCurrentPrice() {
        return displayedCurrentPrice;
    }

    public void setDisplayedCurrentPrice(String displayedCurrentPrice) {
        this.displayedCurrentPrice = displayedCurrentPrice;
    }

}
