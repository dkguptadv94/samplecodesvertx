
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Travelling {

    @Expose
    private List<Object> children;
    @Expose
    private List<Object> extraPeople;
    @Expose
    private List<String> pets;

    public List<Object> getChildren() {
        return children;
    }

    public void setChildren(List<Object> children) {
        this.children = children;
    }

    public List<Object> getExtraPeople() {
        return extraPeople;
    }

    public void setExtraPeople(List<Object> extraPeople) {
        this.extraPeople = extraPeople;
    }

    public List<String> getPets() {
        return pets;
    }

    public void setPets(List<String> pets) {
        this.pets = pets;
    }

}
