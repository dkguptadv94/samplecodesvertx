
package models.properties.proGetDetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class Body {

    private List<Amenity> amenities;
    private AtAGlance atAGlance;
    private GuestReviews guestReviews;
    private HotelBadge hotelBadge;
    private HotelWelcomeRewards hotelWelcomeRewards;
    private Miscellaneous miscellaneous;
    private Overview overview;
    private PageInfo pageInfo;
    private PdpHeader pdpHeader;
    private PropertyDescription propertyDescription;
    private RoomsAndRates roomsAndRates;
    private SmallPrint smallPrint;
    private SpecialFeatures specialFeatures;
    private Boolean trustYouReviewsCredit;
    private HygieneAndCleanliness hygieneAndCleanliness;

}
