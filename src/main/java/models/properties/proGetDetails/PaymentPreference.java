
package models.properties.proGetDetails;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PaymentPreference {

    @Expose
    private Options options;
    @Expose
    private RoomTracking roomTracking;

    public Options getOptions() {
        return options;
    }

    public void setOptions(Options options) {
        this.options = options;
    }

    public RoomTracking getRoomTracking() {
        return roomTracking;
    }

    public void setRoomTracking(RoomTracking roomTracking) {
        this.roomTracking = roomTracking;
    }

}
