
package models.properties.proGetHotelPic;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class HotelImage {

    private String baseUrl;
    private Long imageId;
    private String mediaGUID;
    private List<Size> sizes;
    private TrackingDetails trackingDetails;

}
