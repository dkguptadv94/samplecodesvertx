
package models.properties.proGetHotelPic;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Image {

    @Expose
    private String baseUrl;
    @Expose
    private Long imageId;
    @Expose
    private String mediaGUID;
    @Expose
    private List<Size> sizes;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public String getMediaGUID() {
        return mediaGUID;
    }

    public void setMediaGUID(String mediaGUID) {
        this.mediaGUID = mediaGUID;
    }

    public List<Size> getSizes() {
        return sizes;
    }

    public void setSizes(List<Size> sizes) {
        this.sizes = sizes;
    }

}
