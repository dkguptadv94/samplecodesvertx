
package models.properties.proGetHotelPic;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ProGetHotelPicResponse {

    @Expose
    private FeaturedImageTrackingDetails featuredImageTrackingDetails;
    @Expose
    private Long hotelId;
    @Expose
    private List<HotelImage> hotelImages;
    @Expose
    private PropertyImageTrackingDetails propertyImageTrackingDetails;
    @Expose
    private List<RoomImage> roomImages;

    public FeaturedImageTrackingDetails getFeaturedImageTrackingDetails() {
        return featuredImageTrackingDetails;
    }

    public void setFeaturedImageTrackingDetails(FeaturedImageTrackingDetails featuredImageTrackingDetails) {
        this.featuredImageTrackingDetails = featuredImageTrackingDetails;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public List<HotelImage> getHotelImages() {
        return hotelImages;
    }

    public void setHotelImages(List<HotelImage> hotelImages) {
        this.hotelImages = hotelImages;
    }

    public PropertyImageTrackingDetails getPropertyImageTrackingDetails() {
        return propertyImageTrackingDetails;
    }

    public void setPropertyImageTrackingDetails(PropertyImageTrackingDetails propertyImageTrackingDetails) {
        this.propertyImageTrackingDetails = propertyImageTrackingDetails;
    }

    public List<RoomImage> getRoomImages() {
        return roomImages;
    }

    public void setRoomImages(List<RoomImage> roomImages) {
        this.roomImages = roomImages;
    }

}
