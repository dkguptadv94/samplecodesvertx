
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class Choice {
    private String label;
    private Boolean selected;
    private String value;

}
