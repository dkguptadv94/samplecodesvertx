
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Deals {

    @Expose
    private Boolean greatRate;
    @Expose
    private String priceReasoning;

    public Boolean getGreatRate() {
        return greatRate;
    }

    public void setGreatRate(Boolean greatRate) {
        this.greatRate = greatRate;
    }

    public String getPriceReasoning() {
        return priceReasoning;
    }

    public void setPriceReasoning(String priceReasoning) {
        this.priceReasoning = priceReasoning;
    }

}
