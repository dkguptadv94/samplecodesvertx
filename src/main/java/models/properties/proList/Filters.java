
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Filters {

    private Accessibility accessibility;
    //private AccommodationType accommodationType;
    private Boolean applied;
    private Facilities facilities;
    private GuestRating guestRating;
    private Landmarks landmarks;
    private Name name;
    private Neighbourhood neighbourhood;
    private PaymentPreference paymentPreference;
    private Price price;
    private StarRating starRating;
    private ThemesAndTypes themesAndTypes;
    private WelcomeRewards welcomeRewards;

}
