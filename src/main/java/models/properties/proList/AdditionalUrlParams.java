
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AdditionalUrlParams {

    @SerializedName("destination-id")
    private String destinationId;
    @SerializedName("q-destination")
    private String qDestination;
    @SerializedName("resolved-location")
    private String resolvedLocation;

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getQDestination() {
        return qDestination;
    }

    public void setQDestination(String qDestination) {
        this.qDestination = qDestination;
    }

    public String getResolvedLocation() {
        return resolvedLocation;
    }

    public void setResolvedLocation(String resolvedLocation) {
        this.resolvedLocation = resolvedLocation;
    }

}
