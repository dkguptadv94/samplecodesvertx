
package models.properties.proList;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
@Data
public class SearchResults {

    @Expose
    private Pagination pagination;
    @Expose
    private List<Result> results;
    @Expose
    private Long totalCount;
}
