
package models.properties.proList;

import javax.annotation.Generated;
import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class EnhancedChoice {
    public String label;
    public String itemMeta;
    public List<Choices> choices;

}
