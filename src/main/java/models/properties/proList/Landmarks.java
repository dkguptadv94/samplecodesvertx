
package models.properties.proList;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Landmarks {

    @Expose
    private List<Object> distance;
    @Expose
    private List<Item> items;
    @Expose
    private List<Object> selectedOrder;

    public List<Object> getDistance() {
        return distance;
    }

    public void setDistance(List<Object> distance) {
        this.distance = distance;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<Object> getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(List<Object> selectedOrder) {
        this.selectedOrder = selectedOrder;
    }

}
