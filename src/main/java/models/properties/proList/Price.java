
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Price {

    @Expose
    private String additionalInfo;
    @Expose
    private String current;
    @Expose
    private Double exactCurrent;
    @Expose
    private String info;
    @Expose
    private String label;
    @Expose
    private Long multiplier;
    @Expose
    private String old;
    @Expose
    private Range range;
    @Expose
    private String totalPricePerStay;

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public Double getExactCurrent() {
        return exactCurrent;
    }

    public void setExactCurrent(Double exactCurrent) {
        this.exactCurrent = exactCurrent;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Long multiplier) {
        this.multiplier = multiplier;
    }

    public String getOld() {
        return old;
    }

    public void setOld(String old) {
        this.old = old;
    }

    public Range getRange() {
        return range;
    }

    public void setRange(Range range) {
        this.range = range;
    }

    public String getTotalPricePerStay() {
        return totalPricePerStay;
    }

    public void setTotalPricePerStay(String totalPricePerStay) {
        this.totalPricePerStay = totalPricePerStay;
    }

}
