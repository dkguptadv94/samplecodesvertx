
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Autosuggest {

    @Expose
    private AdditionalUrlParams additionalUrlParams;

    public AdditionalUrlParams getAdditionalUrlParams() {
        return additionalUrlParams;
    }

    public void setAdditionalUrlParams(AdditionalUrlParams additionalUrlParams) {
        this.additionalUrlParams = additionalUrlParams;
    }

}
