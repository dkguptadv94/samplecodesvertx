
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@lombok.Data
public class ProListResponse {

    private Data data;
    private String result;

}
