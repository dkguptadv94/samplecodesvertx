
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Tracking {

    @Expose
    private Omniture omniture;

    public Omniture getOmniture() {
        return omniture;
    }

    public void setOmniture(Omniture omniture) {
        this.omniture = omniture;
    }

}
