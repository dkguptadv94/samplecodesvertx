
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class Body {

    private Filters filters;
    private String header;
    private Miscellaneous miscellaneous;
    private PageInfo pageInfo;
    private PointOfSale pointOfSale;
    private Query query;
    private SearchResults searchResults;
    private SortResults sortResults;

}
