
package models.properties.proList;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
@Data
public class Result {

    @Expose
    private Address address;
    @Expose
    private Badging badging;
    @Expose
    private Coordinate coordinate;
    @Expose
    private String coupon;
    @Expose
    private Deals deals;
    @Expose
    private GuestReviews guestReviews;
    @Expose
    private Long id;
    @Expose
    private List<Landmark> landmarks;
    @Expose
    private Messaging messaging;
    @Expose
    private String name;
    @Expose
    private String neighbourhood;
    @Expose
    private String pimmsAttributes;
    @Expose
    private String providerType;
    @Expose
    private RatePlan ratePlan;
    @Expose
    private Long roomsLeft;
    @Expose
    private Double starRating;
    @Expose
    private Long supplierHotelId;
    @Expose
    private String thumbnailUrl;
    @Expose
    private TripAdvisorGuestReviews tripAdvisorGuestReviews;
    @Expose
    private WelcomeRewards welcomeRewards;
    @Expose
    private OptimizedThumbUrl optimizedThumbUrls;
}
