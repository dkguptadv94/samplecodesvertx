
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Miscellaneous {

    @Expose
    private String pageViewBeaconUrl;

    public String getPageViewBeaconUrl() {
        return pageViewBeaconUrl;
    }

    public void setPageViewBeaconUrl(String pageViewBeaconUrl) {
        this.pageViewBeaconUrl = pageViewBeaconUrl;
    }

}
