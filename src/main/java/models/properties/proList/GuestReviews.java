
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
@Data
public class GuestReviews {

    @Expose
    private Double rating;
    @Expose
    private Long scale;
    @Expose
    private Long total;
    @Expose
    private Double unformattedRating;
}
