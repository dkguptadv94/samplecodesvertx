
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Pagination {

    @Expose
    private Long currentPage;
    @Expose
    private String nextPageGroup;
    @Expose
    private Long nextPageNumber;
    @Expose
    private String pageGroup;

    public Long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    public String getNextPageGroup() {
        return nextPageGroup;
    }

    public void setNextPageGroup(String nextPageGroup) {
        this.nextPageGroup = nextPageGroup;
    }

    public Long getNextPageNumber() {
        return nextPageNumber;
    }

    public void setNextPageNumber(Long nextPageNumber) {
        this.nextPageNumber = nextPageNumber;
    }

    public String getPageGroup() {
        return pageGroup;
    }

    public void setPageGroup(String pageGroup) {
        this.pageGroup = pageGroup;
    }

}
