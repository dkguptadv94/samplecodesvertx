
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Range {

    @Expose
    private Long increments;
    @Expose
    private Max max;
    @Expose
    private Min min;

    public Long getIncrements() {
        return increments;
    }

    public void setIncrements(Long increments) {
        this.increments = increments;
    }

    public Max getMax() {
        return max;
    }

    public void setMax(Max max) {
        this.max = max;
    }

    public Min getMin() {
        return min;
    }

    public void setMin(Min min) {
        this.min = min;
    }

}
