
package models.properties.proList;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class Option {
    private List<Choice> choices;
    private List<EnhancedChoice> enhancedChoices;
    private String itemMeta;
    private String label;

}
