
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class Choices {

    @Expose
    private Double id;
    @Expose
    private String label;


}
