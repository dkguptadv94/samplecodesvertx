
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Omniture {

    @SerializedName("s.currencyCode")
    private String sCurrencyCode;
    @SerializedName("s.eVar10")
    private String sEVar10;
    @SerializedName("s.eVar105")
    private String sEVar105;
    @SerializedName("s.eVar13")
    private String sEVar13;
    @SerializedName("s.eVar16")
    private String sEVar16;
    @SerializedName("s.eVar2")
    private String sEVar2;
    @SerializedName("s.eVar22")
    private String sEVar22;
    @SerializedName("s.eVar23")
    private String sEVar23;
    @SerializedName("s.eVar24")
    private String sEVar24;
    @SerializedName("s.eVar29")
    private String sEVar29;
    @SerializedName("s.eVar3")
    private String sEVar3;
    @SerializedName("s.eVar31")
    private String sEVar31;
    @SerializedName("s.eVar32")
    private String sEVar32;
    @SerializedName("s.eVar33")
    private String sEVar33;
    @SerializedName("s.eVar34")
    private String sEVar34;
    @SerializedName("s.eVar4")
    private String sEVar4;
    @SerializedName("s.eVar40")
    private String sEVar40;
    @SerializedName("s.eVar41")
    private String sEVar41;
    @SerializedName("s.eVar42")
    private String sEVar42;
    @SerializedName("s.eVar43")
    private String sEVar43;
    @SerializedName("s.eVar5")
    private String sEVar5;
    @SerializedName("s.eVar6")
    private String sEVar6;
    @SerializedName("s.eVar63")
    private String sEVar63;
    @SerializedName("s.eVar69")
    private String sEVar69;
    @SerializedName("s.eVar7")
    private String sEVar7;
    @SerializedName("s.eVar9")
    private String sEVar9;
    @SerializedName("s.eVar93")
    private String sEVar93;
    @SerializedName("s.eVar95")
    private String sEVar95;
    @SerializedName("s.products")
    private String sProducts;
    @SerializedName("s.prop14")
    private String sProp14;
    @SerializedName("s.prop15")
    private String sProp15;
    @SerializedName("s.prop18")
    private String sProp18;
    @SerializedName("s.prop19")
    private String sProp19;
    @SerializedName("s.prop2")
    private String sProp2;
    @SerializedName("s.prop20")
    private String sProp20;
    @SerializedName("s.prop21")
    private String sProp21;
    @SerializedName("s.prop22")
    private String sProp22;
    @SerializedName("s.prop27")
    private String sProp27;
    @SerializedName("s.prop29")
    private String sProp29;
    @SerializedName("s.prop3")
    private String sProp3;
    @SerializedName("s.prop32")
    private String sProp32;
    @SerializedName("s.prop33")
    private String sProp33;
    @SerializedName("s.prop36")
    private String sProp36;
    @SerializedName("s.prop5")
    private String sProp5;
    @SerializedName("s.prop7")
    private String sProp7;
    @SerializedName("s.prop74")
    private String sProp74;
    @SerializedName("s.prop8")
    private String sProp8;
    @SerializedName("s.prop9")
    private String sProp9;
    @SerializedName("s.server")
    private String sServer;

    public String getSCurrencyCode() {
        return sCurrencyCode;
    }

    public void setSCurrencyCode(String sCurrencyCode) {
        this.sCurrencyCode = sCurrencyCode;
    }

    public String getSEVar10() {
        return sEVar10;
    }

    public void setSEVar10(String sEVar10) {
        this.sEVar10 = sEVar10;
    }

    public String getSEVar105() {
        return sEVar105;
    }

    public void setSEVar105(String sEVar105) {
        this.sEVar105 = sEVar105;
    }

    public String getSEVar13() {
        return sEVar13;
    }

    public void setSEVar13(String sEVar13) {
        this.sEVar13 = sEVar13;
    }

    public String getSEVar16() {
        return sEVar16;
    }

    public void setSEVar16(String sEVar16) {
        this.sEVar16 = sEVar16;
    }

    public String getSEVar2() {
        return sEVar2;
    }

    public void setSEVar2(String sEVar2) {
        this.sEVar2 = sEVar2;
    }

    public String getSEVar22() {
        return sEVar22;
    }

    public void setSEVar22(String sEVar22) {
        this.sEVar22 = sEVar22;
    }

    public String getSEVar23() {
        return sEVar23;
    }

    public void setSEVar23(String sEVar23) {
        this.sEVar23 = sEVar23;
    }

    public String getSEVar24() {
        return sEVar24;
    }

    public void setSEVar24(String sEVar24) {
        this.sEVar24 = sEVar24;
    }

    public String getSEVar29() {
        return sEVar29;
    }

    public void setSEVar29(String sEVar29) {
        this.sEVar29 = sEVar29;
    }

    public String getSEVar3() {
        return sEVar3;
    }

    public void setSEVar3(String sEVar3) {
        this.sEVar3 = sEVar3;
    }

    public String getSEVar31() {
        return sEVar31;
    }

    public void setSEVar31(String sEVar31) {
        this.sEVar31 = sEVar31;
    }

    public String getSEVar32() {
        return sEVar32;
    }

    public void setSEVar32(String sEVar32) {
        this.sEVar32 = sEVar32;
    }

    public String getSEVar33() {
        return sEVar33;
    }

    public void setSEVar33(String sEVar33) {
        this.sEVar33 = sEVar33;
    }

    public String getSEVar34() {
        return sEVar34;
    }

    public void setSEVar34(String sEVar34) {
        this.sEVar34 = sEVar34;
    }

    public String getSEVar4() {
        return sEVar4;
    }

    public void setSEVar4(String sEVar4) {
        this.sEVar4 = sEVar4;
    }

    public String getSEVar40() {
        return sEVar40;
    }

    public void setSEVar40(String sEVar40) {
        this.sEVar40 = sEVar40;
    }

    public String getSEVar41() {
        return sEVar41;
    }

    public void setSEVar41(String sEVar41) {
        this.sEVar41 = sEVar41;
    }

    public String getSEVar42() {
        return sEVar42;
    }

    public void setSEVar42(String sEVar42) {
        this.sEVar42 = sEVar42;
    }

    public String getSEVar43() {
        return sEVar43;
    }

    public void setSEVar43(String sEVar43) {
        this.sEVar43 = sEVar43;
    }

    public String getSEVar5() {
        return sEVar5;
    }

    public void setSEVar5(String sEVar5) {
        this.sEVar5 = sEVar5;
    }

    public String getSEVar6() {
        return sEVar6;
    }

    public void setSEVar6(String sEVar6) {
        this.sEVar6 = sEVar6;
    }

    public String getSEVar63() {
        return sEVar63;
    }

    public void setSEVar63(String sEVar63) {
        this.sEVar63 = sEVar63;
    }

    public String getSEVar69() {
        return sEVar69;
    }

    public void setSEVar69(String sEVar69) {
        this.sEVar69 = sEVar69;
    }

    public String getSEVar7() {
        return sEVar7;
    }

    public void setSEVar7(String sEVar7) {
        this.sEVar7 = sEVar7;
    }

    public String getSEVar9() {
        return sEVar9;
    }

    public void setSEVar9(String sEVar9) {
        this.sEVar9 = sEVar9;
    }

    public String getSEVar93() {
        return sEVar93;
    }

    public void setSEVar93(String sEVar93) {
        this.sEVar93 = sEVar93;
    }

    public String getSEVar95() {
        return sEVar95;
    }

    public void setSEVar95(String sEVar95) {
        this.sEVar95 = sEVar95;
    }

    public String getSProducts() {
        return sProducts;
    }

    public void setSProducts(String sProducts) {
        this.sProducts = sProducts;
    }

    public String getSProp14() {
        return sProp14;
    }

    public void setSProp14(String sProp14) {
        this.sProp14 = sProp14;
    }

    public String getSProp15() {
        return sProp15;
    }

    public void setSProp15(String sProp15) {
        this.sProp15 = sProp15;
    }

    public String getSProp18() {
        return sProp18;
    }

    public void setSProp18(String sProp18) {
        this.sProp18 = sProp18;
    }

    public String getSProp19() {
        return sProp19;
    }

    public void setSProp19(String sProp19) {
        this.sProp19 = sProp19;
    }

    public String getSProp2() {
        return sProp2;
    }

    public void setSProp2(String sProp2) {
        this.sProp2 = sProp2;
    }

    public String getSProp20() {
        return sProp20;
    }

    public void setSProp20(String sProp20) {
        this.sProp20 = sProp20;
    }

    public String getSProp21() {
        return sProp21;
    }

    public void setSProp21(String sProp21) {
        this.sProp21 = sProp21;
    }

    public String getSProp22() {
        return sProp22;
    }

    public void setSProp22(String sProp22) {
        this.sProp22 = sProp22;
    }

    public String getSProp27() {
        return sProp27;
    }

    public void setSProp27(String sProp27) {
        this.sProp27 = sProp27;
    }

    public String getSProp29() {
        return sProp29;
    }

    public void setSProp29(String sProp29) {
        this.sProp29 = sProp29;
    }

    public String getSProp3() {
        return sProp3;
    }

    public void setSProp3(String sProp3) {
        this.sProp3 = sProp3;
    }

    public String getSProp32() {
        return sProp32;
    }

    public void setSProp32(String sProp32) {
        this.sProp32 = sProp32;
    }

    public String getSProp33() {
        return sProp33;
    }

    public void setSProp33(String sProp33) {
        this.sProp33 = sProp33;
    }

    public String getSProp36() {
        return sProp36;
    }

    public void setSProp36(String sProp36) {
        this.sProp36 = sProp36;
    }

    public String getSProp5() {
        return sProp5;
    }

    public void setSProp5(String sProp5) {
        this.sProp5 = sProp5;
    }

    public String getSProp7() {
        return sProp7;
    }

    public void setSProp7(String sProp7) {
        this.sProp7 = sProp7;
    }

    public String getSProp74() {
        return sProp74;
    }

    public void setSProp74(String sProp74) {
        this.sProp74 = sProp74;
    }

    public String getSProp8() {
        return sProp8;
    }

    public void setSProp8(String sProp8) {
        this.sProp8 = sProp8;
    }

    public String getSProp9() {
        return sProp9;
    }

    public void setSProp9(String sProp9) {
        this.sProp9 = sProp9;
    }

    public String getSServer() {
        return sServer;
    }

    public void setSServer(String sServer) {
        this.sServer = sServer;
    }

}
