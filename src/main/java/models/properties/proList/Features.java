
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Features {

    @Expose
    private Boolean freeCancellation;
    @Expose
    private Boolean noCCRequired;
    @Expose
    private Boolean paymentPreference;

    public Boolean getFreeCancellation() {
        return freeCancellation;
    }

    public void setFreeCancellation(Boolean freeCancellation) {
        this.freeCancellation = freeCancellation;
    }

    public Boolean getNoCCRequired() {
        return noCCRequired;
    }

    public void setNoCCRequired(Boolean noCCRequired) {
        this.noCCRequired = noCCRequired;
    }

    public Boolean getPaymentPreference() {
        return paymentPreference;
    }

    public void setPaymentPreference(Boolean paymentPreference) {
        this.paymentPreference = paymentPreference;
    }

}
