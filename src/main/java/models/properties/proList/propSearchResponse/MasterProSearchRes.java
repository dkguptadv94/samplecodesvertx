package models.properties.proList.propSearchResponse;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MasterProSearchRes {
    private List<Property> properties=new ArrayList<>();
}
