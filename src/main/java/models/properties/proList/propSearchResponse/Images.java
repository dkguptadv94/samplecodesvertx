package models.properties.proList.propSearchResponse;

import lombok.Data;

@Data
public class Images {
    private String title;
    private String url;
}
