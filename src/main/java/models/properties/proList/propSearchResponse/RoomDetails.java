package models.properties.proList.propSearchResponse;

import lombok.Data;
import models.properties.proGetDetails.Cancellation;

@Data
public class RoomDetails {
    private String description;
    private String roomId;
    private String image;
    private String price;
    private String title;

    private String rateId;
    private Cancellation cancellation;

}
