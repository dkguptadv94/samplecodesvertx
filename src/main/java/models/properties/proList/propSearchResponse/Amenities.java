package models.properties.proList.propSearchResponse;

import lombok.Data;

@Data
public class Amenities {
    private Boolean ac = false;
    private Boolean wifi = false;
    private Boolean pool = false;
    private Boolean food = false;
    private Boolean bar = false;
}
