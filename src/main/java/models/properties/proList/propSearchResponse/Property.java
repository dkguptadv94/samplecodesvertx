package models.properties.proList.propSearchResponse;

import lombok.Data;
import models.properties.proList.Coordinate;

import java.util.List;

@Data
public class Property {
    private String propertyId;
    private String name;
    private String address;
    private Amenities amenities;
    private Coordinate coordinate;
    private Double currentPrice;
    private Long roomLeft;
    private String thumbnailImage;
    private Long starRating;
    private Double userRating;


    private String description;

    private List<RoomDetails> rooms;
    private List<Images> images;

}
