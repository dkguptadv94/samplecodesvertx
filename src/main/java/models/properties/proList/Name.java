
package models.properties.proList;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Name {

    @Expose
    private Autosuggest autosuggest;
    @Expose
    private Item item;

    public Autosuggest getAutosuggest() {
        return autosuggest;
    }

    public void setAutosuggest(Autosuggest autosuggest) {
        this.autosuggest = autosuggest;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

}
